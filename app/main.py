"""
This is a template web server for ska-devops related jobs
"""
import logging
import os
import sys

import yaml
from fastapi import FastAPI, Security

from app.models.Authentication import Authentication

logger = logging.getLogger("gunicorn.error")

app = FastAPI()

logger.info("JSON_CONFIG_PATH: %s", os.environ["JSON_CONFIG_PATH"])

plugins = []
with open(os.environ["JSON_CONFIG_PATH"]) as f:
    plugins = yaml.safe_load(f)


@app.get("/")
def test():
    return 200


logger.info("########### Plugins: \n%s", plugins)

for plugin in plugins["plugins"]:
    mod = __import__(
        plugin["router"]["module"], fromlist=[plugin["router"]["class"]]
    )
    klass = getattr(mod, plugin["router"]["class"])

    authentication = Authentication()

    # Get Authentication type and ENV variable names
    auth_type = plugin.get("auth_type")
    token_env = plugin.get("token_env")
    token_header_env = plugin.get("token_header_env")
    username_env = plugin.get("username_env")
    password_env = plugin.get("password_env")
    hmac_secret_env = plugin.get("hmac_secret_env")
    hmac_signature_header = plugin.get("hmac_signature_header")

    # retrieve authentication callable function based on auth_type

    try:
        authFunction = authentication.init(
            auth_type,
            token_env,
            token_header_env,
            username_env,
            password_env,
            hmac_secret_env,
            hmac_signature_header,
        )
    except AssertionError as e:
        sys.exit(
            "ERROR  Bad config on router " + plugin.get("name") + ": " + str(e)
        )

    # initiate router with the authentication dependency
    app.include_router(
        klass.router,
        prefix=plugin["prefix"],
        tags=plugin["tags"],
        dependencies=[Security(authFunction)],
        responses={404: {"description": "Not found"}},
    )
