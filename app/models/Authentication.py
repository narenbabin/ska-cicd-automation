import base64
import hashlib
import hmac
import logging
import os

from fastapi import HTTPException, Request, status
from fastapi.security.api_key import APIKeyHeader

logger = logging.getLogger("gunicorn.error")


class Authentication:
    def __init__(self):
        logger.info("Auth class created")
        self.auth_info = {}

    def init(
        self,
        auth_type,
        token_env,
        token_header_env,
        username_env,
        password_env,
        hmac_secret_env,
        hmac_signature_header,
    ):
        assert (
            auth_type is not None
        ), "Authentication Type (auth_type) is not found on plugins.conf.yaml"

        if auth_type == "token":

            assert token_header_env is not None, (
                "Token Header (token_header_env) is not found"
                " on plugins.conf.yaml"
            )
            assert (
                token_env is not None
            ), "Token (token_env) is not found on plugins.conf.yaml"

            token_header = os.getenv(token_header_env)
            token = os.getenv(token_env)

            assert (
                token_header is not None
            ), "Token Header env variable is empty"

            assert token is not None, "Token env variable is empty"

            self.auth_info["token_header"] = token_header
            self.auth_info["token"] = token

            return self.with_token

        elif auth_type == "password":
            assert (
                username_env is not None
            ), "Username (username_env) is not found on plugins.conf.yaml"
            assert (
                password_env is not None
            ), "Password (password_env) is not found on plugins.conf.yaml"

            username = os.getenv(username_env)
            password = os.getenv(password_env)

            assert (
                username is not None
            ), "Username Header env variable is empty"

            assert password is not None, "Password env variable is empty"

            self.auth_info["username"] = username
            self.auth_info["password"] = password

            return self.with_password

        elif auth_type == "hmac_signature":

            assert hmac_signature_header is not None, (
                "HMAC_SIGNATURE Header (hmac_signature_header) is not found"
                " on plugins.conf.yaml"
            )
            assert hmac_secret_env is not None, (
                "HMAC_SIGNATURE (hmac_secret_env) is not found "
                "on plugins.conf.yaml"
            )

            hmac_secret = os.getenv(hmac_secret_env)

            assert (
                hmac_signature_header is not None
            ), "HMAC signature header variable is empty"

            assert hmac_secret is not None, "hmac_secret env variable is empty"

            self.auth_info["hmac_signature_header"] = hmac_signature_header
            self.auth_info["hmac_secret"] = hmac_secret

            return self.with_hmac_signature

        elif auth_type == "none":
            return self.no_auth

    async def with_token(self, request: Request):

        # Retrives header parameter
        key_retriever = APIKeyHeader(
            name=self.auth_info["token_header"],
            auto_error=True,
        )

        token: str = await key_retriever(request)

        if token != self.auth_info["token"]:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid API Key",
            )

    async def with_password(self, request: Request):
        logger.info("%s auth process initiate")

        # Retrives header parameter
        auth_code: str = request.headers.get("authorization")
        assert auth_code is not None, "Bad basic authorization request"

        user_password_coded = auth_code.split(" ")[1]
        assert (
            user_password_coded is not None
        ), "Bad basic authorization request"

        user_password_bytes: str = user_password_coded.encode("utf-8")
        user_password: str = base64.b64decode(user_password_bytes).decode(
            "utf-8"
        )

        username = user_password.split(":")[0]
        password = user_password.split(":")[1]

        if (
            username != self.auth_info["username"]
            or password != self.auth_info["password"]
        ):
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid username and/or password",
            )

    async def with_hmac_signature(self, request: Request):

        # Retrieves header parameter
        key_retriever = APIKeyHeader(
            name=self.auth_info["hmac_signature_header"],
            auto_error=True,
        )

        hmac_signature: str = await key_retriever(request)
        logger.debug("Retrieved HMAC Signature: %s", hmac_signature)

        rq_body = await request.body()
        logger.debug("Body: %s", rq_body)
        logger.debug("Body type: %s", type(rq_body))

        secret_encoded = bytes(self.auth_info["hmac_secret"], "utf-8")

        # Create digest from request body
        digest_maker = hmac.new(
            secret_encoded,
            rq_body,
            hashlib.sha1,
        )
        digest = digest_maker.hexdigest()

        if hmac_signature != digest:
            logger.debug("Digest: %s", digest)
            logger.debug("Signature received: %s", hmac_signature)
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid HMAC Signature or corrupted payload received",
            )

    def no_auth(self):
        pass
