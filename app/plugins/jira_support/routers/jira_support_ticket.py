import logging

from fastapi import Request
from fastapi.routing import APIRouter

from app.plugins.jira_support.models.slack_handler import app_handler

logging.basicConfig(level=logging.DEBUG)

router = APIRouter()


@router.post("/message_shortcuts")
async def message_shortcut(req: Request):
    return await app_handler.handle(req)


@router.post("/slack/events")
async def endpoint(req: Request):
    return await app_handler.handle(req)


@router.post("/options/slack/events")
async def menu(req: Request):
    return await app_handler.handle(req)


# For development (before running in k8s), run this main
# if __name__ == "__main__":
#     import uvicorn

#     uvicorn.run(router, host="0.0.0.0", port=8000)
