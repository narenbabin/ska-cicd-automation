# SKA Slack Integration

## Environment
To develop a Slack app, it is recommended to create your own Slack workspace and test against it. 

### Marvin Use Case
If the Slack app you are working on is Marvin itself, then you can use a pre-made Slack app named [*Marvin-Test*](https://api.slack.com/apps/A02CE9A62DN) to conduct your code changes, without the need for setting up your own Slack workspace.

In that case, you should use the following `.env` variables from the *Marvin-Test* app.

```
SLACK_BOT_TOKEN=...
SLACK_SIGNING_SECRET=...
```

Following the steps in this repository README, start your local testing with `make development` and expose it outside your local development machine.
You can do so using [ngrok](https://ngrok.com/) for instance: `ngrok http 3000`

Change the various [*Marvin-Test*](https://api.slack.com/apps/A02CE9A62DN) endpoints to point at your local deployment (i.e.: `https://da0dd48a7db7.ngrok.io/jira/support/slack/events`)

You should now have the *Marvin-Test* Slack app connected to your local development.

## Slack Bolt API
This plugin was developed using a FastAPI implementation of the Slack Bolt framework. The documentation is available on a [thorough tutorial](https://slack.dev/bolt-python/tutorial/getting-started). 

Note that our plugin uses asynchronous methods, which are available as part of the Bolt SDK.

Example apps are available on the [Bolt Github repository](https://github.com/slackapi/bolt-python).

## Plugin Features
This plugin uses two integration points for Slack: the Slack Events API, and the Message Shortcuts integration.

### Slack Events
The Bolt API listens on the `/slack/events/` endpoint, and redirects all traffic through this endpoint. On Slack, we can however configure certain Slack Events to trigger specific functionality. For more information, visit the [Slack documentation on Events](https://slack.dev/bolt-python/concepts#event-listening).

For this example app we listen for the `@mention` of the bot and respond with a simple message.

### Message Shortcuts
Slack provides two different shortcuts: a Global shortcut menu, and a Message shortcut menu. For the Jira Support Issue plugin, the idea was to use the message contents to generate some of the contents of the Jira issue automatically, so that the user can have a pre-populated form and quickly submit the data to Jira. It therefore made more sense to use the Message Shortcut.

A modal is opened using the list of users that is used for populating the drop-down menu, for authorization checking first (the user opening the modal should be on the list of users on the sheet). This list of users is created using a Google Sheet as [external data source](https://api.slack.com/reference/block-kit/block-elements#external_select) (see below section). The list of projects in which the  is also populated from this spreadsheet.

### Google Sheets
The [Google Sheets API]() provides a rudimentary data source for management of the list of users and projects that can be assigned and populated with Jira tickets, respectively. The API Key and Sheet ID are both stored as environment variables and called in the [handler](app/plugins/jira_support/models/google_sheets_handler.py).

For the *Marvin* Slack app you can request permission to acess the spreadsheet [here](https://docs.google.com/spreadsheets/d/104Y1N7DFnCS2uQYcpVhiZF95hN6XZ9wAbb4IvT5_RDM).

## Plugin implementation
The plugin basically follows the architecture of the SKA CICD framework, but since the Bolt API is also a framework in itself, it is easy to get confused. The plugin has the normal `routers` and `models` directories. All requests are handled by the endpoints declared under `routers/jira_support_ticket`. The asynchronous call to the SlackAppHandler is awaited, and the internal authentication with Slack is handled by the Bolt API. 

Important to note is Slack's requirement to get a response from the Web service within three seconds. This is accomplished by the `ack()` calls found in all the methods in `/models/slack_handler.py`. 

The APIs created to integration with Jira etc are imported and used as with all other plugins.