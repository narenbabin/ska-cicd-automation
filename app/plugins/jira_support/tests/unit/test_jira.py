import json
import logging

import pytest
from fastapi.testclient import TestClient
from pytest_bdd import given, parsers, scenarios, then

from app.main import app
from app.plugins.jira_support.routers.jira_support_ticket import router

# Used to Print
LOGGER = logging.getLogger(__name__)
# Creating a Testclient with main app
client = TestClient(app)
# Creating a Testclient with router members
client_router = TestClient(router)


@given(parsers.parse("Main route"))
def check_server():

    response = client.get("/")
    pytest.confirm = response.status_code


@given(parsers.parse("{method} http request with body {jsonFile} to {target}"))
def http_with_body(method: str, jsonFile: str, target: str):

    method = method.lower()
    if method == "post":
        pytest.confirm = post(target, jsonFile)
    elif method == "get":
        raise Exception("Get cannot be made with Body")


@given(parsers.parse("{method} http request to {target}"))
def http_without_body(method: str, target: str):

    method = method.lower()
    if method == "post":
        pytest.confirm = post(target)
    elif method == "get":
        pytest.confirm = get(target)


@then(parsers.parse("Return code {message}"))
def check_message(message):
    assert int(message) == pytest.confirm


def post(target, file=None):

    if file is not None:
        with open(file, "r") as myfile:
            data = myfile.read()

        obj = json.loads(data)
        response = client_router.post(target, json=obj)
    else:
        response = client_router.post(target)

    if response.status_code == 200:
        return 200
    else:
        pytest.raises("Http Error: " + str(response))


def get(target):

    response = client_router.get(target)
    if response.status_code == 200:
        return response.status_code
    else:
        raise Exception("Target Error - Cannot get response from Target")


scenarios("./jira.feature")
