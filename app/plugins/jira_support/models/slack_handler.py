import logging
import os

from slack_bolt.adapter.starlette.async_handler import AsyncSlackRequestHandler
from slack_bolt.app.async_app import AsyncApp
from slack_sdk.errors import SlackApiError

from app.plugins.jira_support.models.create_issue_action import (
    CreateSupportIssueAction,
)
from app.plugins.jira_support.models.create_message_preview import (
    CreateSupportIssueMessagePreview,
)
from app.plugins.jira_support.models.user_authorization import authorise_user


# Class to hold config variables to pass between methods, needs refactoring
class Config:
    def __init__(self) -> None:
        # Channel Id that the shortcut command is called for
        self.shortcut_channel_id = {}
        self.thread_ts = None


logger = logging.getLogger("gunicorn.error")
logger.info("JSON_CONFIG_PATH: %s", os.environ["JSON_CONFIG_PATH"])

app = AsyncApp()
app_handler = AsyncSlackRequestHandler(app)

config = Config()


@app.event("app_mention")
async def handle_app_mentions(ack, body, client):
    await ack()
    logger.info("REQUEST BODY:\n%s", body)
    channel_id = body["event"]["channel"]
    thread_ts = body["event"]["event_ts"]
    msg = (
        "Here I am, brain the size of the planet, "
        "and you're asking me to send you a slack message..."
    )
    try:
        result = await client.chat_postMessage(
            channel=channel_id,
            thread_ts=thread_ts,
            text=msg,
        )
        logger.info("#### RESULT FOR APP MENTION RESPONSE \n%s", result)

    except SlackApiError as e:
        logger.debug(e)


@app.message_shortcut("jira_support_issue")
async def open_modal(ack, body, client):
    # Acknowledge the command request
    await ack()

    # Create Message Preview result
    trigger_id = body["trigger_id"]
    logger.info("TRIGGER ID: %s", trigger_id)

    channel_id = body["channel"]["id"]
    channel_name = body["channel"]["name"]
    config.thread_ts = body["message_ts"]

    logger.debug("############ MESSAGE SHORTCUT BODY ##########\n%s", body)
    logger.debug(
        "############# MESSAGE TS ##########\n%s\n#####", config.thread_ts
    )

    # Crash and burn - no auth
    user_id = body["user"]["id"]

    logger.info("Message's user: %s", body["user"])

    if not authorise_user(user_id):
        response = (
            "This user is not authorised to use this "
            "shortcut! Only System Team Members can use it."
            " If you are part of the system team, contact"
            " #team-system-support channel"
        )

        await client.chat_postEphemeral(
            channel=channel_id, user=user_id, text=response
        )
        return

    # View payload
    message_preview = CreateSupportIssueMessagePreview(channel_name)

    # Call views_open with the built-in client
    result = await client.views_open(
        # Pass a valid trigger_id within 3 seconds of receiving it
        trigger_id=trigger_id,
        # type="json-pointer:/view",
        view=message_preview.preview(
            description=body["message"]["text"],
            summary=body["message"]["text"][:50] + "...",
            user_id=user_id,
        ),
    )
    logger.debug("##### THIS IS THE RESULT OF THE MESSAGE PREVIEW CALL ###")
    logger.debug(result)
    logger.debug("###### HERE ENDS RESULT OF THE MESSAGE PREVIEW CALL ####")
    # Save Channel Id with view id
    if result.status_code == 200:
        config.shortcut_channel_id[result.data["view"]["id"]] = channel_id
    else:
        response = f"There was an error with your submission: {trigger_id}"
        await client.chat_postEphemeral(
            channel=channel_id, user=user_id, text=response
        )


@app.action("action-issue-assignee")
async def update_modal(ack, body, client):
    # Acknowledge the button request
    await ack()
    logger.info("*&&&&&&&&&&&&**&&&&&&&&&&&&&&\n%s", body["view"])

    # Pass the view_id
    view_id = body["view"]["id"]
    hash = body["view"]["hash"]

    initial_view = body["view"]
    for item in [
        "id",
        "team_id",
        "state",
        "hash",
        "previous_view_id",
        "root_view_id",
        "app_id",
        "app_installed_team_id",
        "bot_id",
    ]:
        removed_item = initial_view.pop(item)
        logger.info("Removed %s", removed_item)
    logger.info("*&&&&&&&&&&&&**&&&&&&&&&&&&&&")

    # TODO Handle update response!
    # Call views_update with the built-in client
    await client.views_update(
        # Pass the view_id
        view_id=view_id,
        # String that represents view state to protect against race conditions
        hash=hash,
        # View payload with updated blocks
        view=body["view"],
    )


@app.action("action-jira-project")
async def update_modal_again(ack, body, client):
    # Acknowledge the button request
    await ack()
    logger.info("*&&&&&&&&&&&&**&&&&&&&&&&&&&&\n%s", body["view"])

    # Pass the view_id
    view_id = body["view"]["id"]
    hash = body["view"]["hash"]

    initial_view = body["view"]
    for item in [
        "id",
        "team_id",
        "state",
        "hash",
        "previous_view_id",
        "root_view_id",
        "app_id",
        "app_installed_team_id",
        "bot_id",
    ]:
        removed_item = initial_view.pop(item)
        logger.info("Removed %s", removed_item)
    logger.info("*&&&&&&&&&&&&**&&&&&&&&&&&&&&")

    # TODO Handle update response!
    # Call views_update with the built-in client
    await client.views_update(
        # Pass the view_id
        view_id=view_id,
        # String that represents view state to protect against race conditions
        hash=hash,
        # View payload with updated blocks
        view=body["view"],
    )


@app.view("view_create_support_issue")
async def handle_submission(ack, body, client, view):

    # Acknowledge the view_submission event and close the modal
    await ack()

    trigger_id = body["trigger_id"]
    logger.info("TRIGGER ID: %s", trigger_id)

    # # Use current state
    state_values = view["state"]["values"]

    from app.plugins.jira_support.models.create_menu_options import (
        map_projects,
        user_options,
    )

    summary = state_values["block-issue-summary"]["action-issue-summary"][
        "value"
    ]
    description = state_values["block-issue-description"][
        "action-issue-description"
    ]["value"]
    project = state_values["block-jira-project"]["action-jira-project"][
        "selected_option"
    ]["value"]
    if (
        state_values["block-issue-assignee"]["action-issue-assignee"][
            "selected_option"
        ]
        is not None
    ):
        slack_id = state_values["block-issue-assignee"][
            "action-issue-assignee"
        ]["selected_option"]["text"]["text"]
        slack_id = slack_id[3:][:-2]
        logging.info("# # # # # # # # # # SLACK USER ID: %s", slack_id)
        jira_username = user_options(slack_id)[1]
    else:
        jira_username = None

    issue_type = map_projects(project)
    logging.info("$ $ $ $ %s", issue_type)

    logger.info("Current State: \n%s", state_values)

    # Connect to JIRA and create issue
    action_executor = CreateSupportIssueAction()

    # Message to send user
    msg = ""
    try:
        jira_result = await action_executor.action(
            proj_id=project,
            issue_type=issue_type,
            issue_summary=summary,
            issue_description=description,
            assignee_username=jira_username,
        )

        logger.info("Jira returned: %s", jira_result)
        # if jira returned an error, modify the result

        error = jira_result[0]["error"]
        if error:
            result = error
            logger.error(result)
            msg = f"There was an error with your submission: {result}"
        else:
            issue_key = jira_result[0]["issue"].key
            logger.info("Issue created: %s", issue_key)
            jira_url = os.environ["JIRA_URL"]
            logger.info("%s", jira_url)
            msg = (
                f"<{jira_url}/browse/{issue_key}|"
                f"{issue_key}> was created succesfully"
            )

    except Exception as e:
        # Handle error
        msg = f"There was an error with your submission: {e}"
    finally:
        # Message the user
        logger.info("Message: %s", msg)
        await client.chat_postMessage(
            channel=config.shortcut_channel_id[view["id"]],
            thread_ts=config.thread_ts,
            text=msg,
            mrkdwn=True,
        )


@app.options("action-jira-project")
async def show_options(ack):

    from app.plugins.jira_support.models.create_menu_options import (
        project_options,
    )

    options = project_options()

    await ack(options=options)


@app.options("action-issue-assignee")
async def show_user_options(ack):

    from app.plugins.jira_support.models.create_menu_options import (
        user_options,
    )

    options = user_options()

    await ack(options=options)


@app.command("/support")
async def handle_slash_command(ack, command, respond, client):
    await ack()
    # Create Message Preview result
    logger.debug(command)
    message_preview = CreateSupportIssueMessagePreview(command["channel_name"])

    # Crash and burn - no auth
    user_id = command["user_id"]
    trigger_id = command["trigger_id"]
    logger.info("TRIGGER ID: %s", trigger_id)
    channel_id = command["channel_id"]

    logger.info("Message's user: %s", command["user_name"])

    if not authorise_user(user_id):
        response = (
            "This user is not authorised to use this "
            "shortcut! Only System Team Members can use it."
            " If you are part of the system team, contact"
            " #team-system-support channel"
        )
        await respond(response, response_type="ephemeral")
        return

    # View payload
    message_preview = CreateSupportIssueMessagePreview(command["channel_name"])

    # Call views_open with the built-in client
    result = await client.views_open(
        # Pass a valid trigger_id within 3 seconds of receiving it
        trigger_id=trigger_id,
        # type="json-pointer:/view",
        view=message_preview.preview(
            description=command["text"],
            summary=command["text"][:50] + "...",
            user_id=user_id,
        ),
    )
    logger.debug(result)
    # Save Channel Id with view id
    if result.status_code == 200:
        config.shortcut_channel_id[result.data["view"]["id"]] = channel_id
    else:
        response = f"There was an error with your submission: {trigger_id}"
        await client.chat_postEphemeral(
            channel=channel_id, user=user_id, text=response
        )
