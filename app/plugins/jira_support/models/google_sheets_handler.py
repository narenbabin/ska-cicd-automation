from __future__ import print_function

import logging
import os.path

from googleapiclient.discovery import build

GOOGLE_API_KEY = os.environ["GOOGLE_API_KEY"]
SPREADSHEET_ID = os.environ["GOOGLE_SPREADSHEET_ID"]

# If modifying these scopes, delete the file token.json.
SCOPES = ["https://www.googleapis.com/auth/spreadsheets.readonly"]

SPREADSHEET_ID = "12JbxkmXYmNrQl9HJ4e-w6fCnvtNNKdYGVqZcaKn5XSo"

SLACK_USERS_RANGE = "s!A1:B"
JIRA_PROJECT_RANGE = "p!A1:C"

logger = logging.getLogger("gunicorn.error")


class GoogleSheet:
    def __init__(self):
        self.service = build("sheets", "v4", developerKey=GOOGLE_API_KEY)

        # Call the Sheets API
        self.sheet = self.service.spreadsheets()

    def get_values(self, range):
        values = (
            self.sheet.values()
            .get(spreadsheetId=SPREADSHEET_ID, range=range)
            .execute()
            .get("values", [])
        )
        return values

    def get_users(self):
        """Connects to Google Sheets API
        Returns JSON with Slack and JIRA Username mappings,
        JSON: {
            "jira_username": "slack_id",
            "jira_username": "slack_id",
        }
        """
        table = {}
        for value in self.get_values(SLACK_USERS_RANGE):
            table[value[1]] = value[0]
        logger.info("########### TABLE:\n%s", table)
        return table

    def get_projects_channels(self):
        """Get a map of projects and channels from
        the Google Sheets API
        JSON: {
            "channel_id": "jira_project_key",
            "channel_id": "jira_project_key",
        }
        """
        table = {}
        for value in self.get_values(JIRA_PROJECT_RANGE):
            table[value[1]] = value[0]
        logger.info("########### TABLE:\n%s", table)
        return table

    def get_issue_types(self):

        table = {}
        for value in self.get_values(JIRA_PROJECT_RANGE):
            logger.info("VALUE: %s", value)
            table[value[0]] = value[2]
        return table


if __name__ == "__main__":
    # main()
    sheet = GoogleSheet()
    users = sheet.get_users()
    print(users)
