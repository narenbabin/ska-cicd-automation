from app.plugins.jira_support.models.google_sheets_handler import GoogleSheet


def authorise_user(id: str = None):
    """Authorise users for functionality on Slack app

    :param id: Slack ID of user to be authenticated
    :return: None
    """

    # for jira_username, slack_id in system_team_users.items():
    gs = GoogleSheet()
    users = gs.get_users()
    for jira_username, slack_id in users.items():
        if id == slack_id:
            return jira_username
    return None
