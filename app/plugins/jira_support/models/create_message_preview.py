import logging

from ska_cicd_services_api import jira

logger = logging.getLogger("gunicorn.error")


class CreateSupportIssueMessagePreview:
    def __init__(self, channel_name):
        self.jira = jira.SKAJira()
        self.channel_name = channel_name

    def preview(self, description: str, summary: str, user_id: str):
        initial_user = {
            "text": {"type": "plain_text", "text": "*<@" + user_id + ">*"},
            "value": "DevPortal-Services",
        }

        from app.plugins.jira_support.models.create_menu_options import (
            project_options,
        )

        initial_project = project_options(self.channel_name)[1]

        result = {
            "type": "modal",
            "callback_id": "view_create_support_issue",
            "title": {
                "type": "plain_text",
                "text": "JIRA Support Issue",
            },
            "submit": {"type": "plain_text", "text": "Submit"},
            "close": {"type": "plain_text", "text": "Cancel"},
            "blocks": [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "This will create Jira Issue"
                        " in the *Systems Team* "
                        "Jira project with the following:",
                    },
                },
                {"type": "divider"},
                {
                    "type": "input",
                    "block_id": "block-issue-summary",
                    "element": {
                        "type": "plain_text_input",
                        "action_id": "action-issue-summary",
                        "max_length": 50,
                        "placeholder": {
                            "type": "plain_text",
                            "text": "Please enter a Issue Summary"
                            " (Max 50 chars)",
                        },
                        "initial_value": summary,
                    },
                    "label": {
                        "type": "plain_text",
                        "text": "Issue Summary",
                    },
                },
                {
                    "type": "input",
                    "block_id": "block-issue-description",
                    "element": {
                        "type": "plain_text_input",
                        "action_id": "action-issue-description",
                        "initial_value": description,
                        "multiline": True,
                        "placeholder": {
                            "type": "plain_text",
                            "text": "This should be auto populated",
                        },
                    },
                    "label": {
                        "type": "plain_text",
                        "text": "Issue Description",
                    },
                },
                {
                    "type": "section",
                    "block_id": "block-jira-project",
                    "text": {
                        "type": "mrkdwn",
                        "text": "Pick JIRA Project Key",
                    },
                    "accessory": {
                        "action_id": "action-jira-project",
                        "type": "external_select",
                        "initial_option": initial_project,
                        "placeholder": {
                            "type": "plain_text",
                            "text": "Pick a Project for the Issue",
                        },
                        "min_query_length": 0,
                    },
                },
                {
                    "type": "section",
                    "block_id": "block-issue-assignee",
                    "text": {
                        "type": "mrkdwn",
                        "text": "Pick JIRA User as Assignee",
                    },
                    "accessory": {
                        "action_id": "action-issue-assignee",
                        "type": "external_select",
                        "initial_option": initial_user,
                        "placeholder": {
                            "type": "plain_text",
                            "text": "Assign someone to the issue",
                        },
                        "min_query_length": 0,
                    },
                },
            ],
        }
        return result
