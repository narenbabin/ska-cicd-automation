import logging

from app.plugins.jira_support.models.google_sheets_handler import GoogleSheet

logger = logging.getLogger("gunicorn.error")


def project_options(default_channel_name=None):
    gs = GoogleSheet()
    project_list = gs.get_projects_channels()
    options = []
    for channel_name, jira_project in project_list.items():
        item = {
            "text": {
                "type": "plain_text",
                "text": jira_project + ": #" + channel_name,
            },
            "value": jira_project,
        }
        options.append(item)
        if (
            default_channel_name is not None
            and channel_name == default_channel_name
        ):
            initial_project = item
    if default_channel_name is not None:
        return options, initial_project

    return options


def user_options(default_user_id=None):
    gs = GoogleSheet()
    users_list = gs.get_users()
    options = []
    for jira_username, slack_id in users_list.items():
        item = {
            "text": {"type": "plain_text", "text": "*<@" + slack_id + ">*"},
            "value": jira_username,
        }
        options.append(item)
        if default_user_id is not None and default_user_id == slack_id:
            initial_user = jira_username

    if default_user_id is not None:
        return options, initial_user

    return options


def map_projects(project_id):
    gs = GoogleSheet()
    projects = gs.get_issue_types()
    for project, issue_type in projects.items():

        logger.info("$ $ $ $ %s", project)
        logger.info("$ $ $ $ %s", issue_type)
        if project == project_id:
            return issue_type
