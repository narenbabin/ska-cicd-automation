import logging

from ska_cicd_services_api import jira

logger = logging.getLogger("gunicorn.error")


class CreateSupportIssueAction:
    def __init__(self):
        self.jira = jira.SKAJira()

    async def action(
        self,
        proj_id: str = "ST",
        issue_summary=None,
        issue_description=None,
        issue_type: str = "Enabler",
        assignee_username: str = None,
    ):
        await self.jira.authenticate()
        issue_dict = {
            "project": {"key": proj_id},
            "summary": issue_summary,
            "description": issue_description,
            "issuetype": {"name": issue_type},
            "assignee": {"name": assignee_username},
        }

        logger.info("### ISSUE DICT: \n%s", issue_dict)

        result = await self.jira.create_issues([issue_dict])
        return result
