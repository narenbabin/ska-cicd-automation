""" Nexus Route for managing Nexus web hooks """
import logging

from fastapi import APIRouter, Request
from ska_cicd_artefact_validations.validation.models.input_validator import (
    NexusHookValidator,
    Reporter,
)
from ska_cicd_artefact_validations.validation.tasks.hook_workflow import (
    main_task,
)

logger = logging.getLogger("gunicorn.error")

router = APIRouter()


@router.post("/testAuth")
async def test(request: Request):
    logger.info(request)
    return


@router.post("/repository")
async def new_event(request: Request):
    """Triggered when a new component gets
    uploaded/updated/removed in nexus repository"""

    # We probably don't need the api in checks for these.
    # If that's the case we can remove it. This needs to be investigated!
    body = await request.json()
    reporter = Reporter(__name__)
    validator = NexusHookValidator(reporter)
    hook = await validator.validate(body, "Nexus Webhook New Event")

    if hook:
        if hook["action"] == "CREATED":
            logger.debug(
                "New artefact created: %-s: %s-%s",
                hook["component"]["componentId"],
                hook["component"]["name"],
                hook["component"]["version"],
            )
            main_task.delay(body)
        else:
            logger.info("Component is not checked: Action %s", hook["action"])

    return "OK"
