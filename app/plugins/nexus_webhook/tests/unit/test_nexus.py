# from inspect import signature
import logging
import os

import pytest
from fastapi.testclient import TestClient
from pytest_bdd import given, parsers, scenarios, then, when

from app.main import app
from app.plugins.nexus_webhook.routers.nexus_webhook import router

# Used to Print
logger = logging.getLogger(__name__)
# Creating a Testclient with main app
client = TestClient(app)
# Creating a Testclient with router members
client_router = TestClient(router)


@given(parsers.parse("Main route"))
def check_server():

    response = client.get("/")
    pytest.confirm = response.status_code


@given(
    parsers.parse(
        "Authenticate http request to {target} with {header} header "
        "using secret defined in {hmac_secret_env}"
    )
)
def hmac_auth(
    hmac_secret_env: str,
    header: str,
    target: str,
):

    pytest.header = header
    pytest.target = target

    assert (
        hmac_secret_env is not None
    ), "Signature Header (hmac_secret_env) is not found on plugins.conf.yaml"

    pytest.secret = os.getenv(hmac_secret_env)

    assert pytest.secret is not None, "Secret env variable is empty"


@when(parsers.parse("The signature validity is {is_valid_signature}"))
def send_request(is_valid_signature):
    message_dict = {"message": "Some message sent in the request body"}
    import json

    message_json = json.dumps(message_dict)
    if is_valid_signature == "True":
        import hashlib
        import hmac

        digest_maker = hmac.new(
            bytes(pytest.secret, "utf-8"),
            bytes(str(message_json), "utf-8"),
            hashlib.sha1,
        )
        hmac_signature = digest_maker.hexdigest()
    else:
        hmac_signature = str({"message": "not a correct signature"})

    response = client.post(
        pytest.target,
        headers={pytest.header: hmac_signature},
        json=message_dict,
    )

    pytest.confirm = response.status_code


@then(parsers.parse("Return code {message}"))
def check_message(message):
    assert int(message) == pytest.confirm


scenarios("./nexus.feature")
