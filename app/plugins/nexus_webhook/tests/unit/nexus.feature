Feature: Test Web Functionality

    Scenario: Check Server
        Given Main route
        Then  Return code 200

    Scenario: Check an authorized Authentication with http header hmac signature
        Given Authenticate http request to /nexus/webhooks/testAuth with X-Nexus-Webhook-Signature header using secret defined in NEXUS_HMAC_SIGNATURE_SECRET
        When The signature validity is True
        Then Return code 200

    Scenario: Check an unauthorized Authentication with wrong http header hmac signature
        Given Authenticate http request to /nexus/webhooks/testAuth with X-Nexus-Webhook-Signature header using secret defined in NEXUS_HMAC_SIGNATURE_SECRET
        When The signature validity is False
        Then Return code 401
