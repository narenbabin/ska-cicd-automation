# SKA CI/CD Automation Services Nexus Webhook for Artefact Management

This plugin is used to manage the SKA artefacts by checking them against a series of checks and moving them to quarantine repositories.

It uses FastAPI to create webhook that can be set to listen for the Nexus Repository Webhook Events.

Then the plugin deletages the checks to celery workers by creating a task in the queue. [The Artefact Validation Engine](https://gitlab.com/ska-telescope/sdi/ska-cicd-artefact-validations) and all the checks and actions could be found on the repository which is imported here.

To activate the slack reports for input validation alerts, the `SLACK_WEBHOOK_URL` should be set with an incoming webhook url (currently set in the project variables for the pipelines).
