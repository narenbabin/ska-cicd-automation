import logging
import re

from ska_cicd_services_api.gitlab import GitLabApi
from ska_cicd_services_api.readthedocs import HTTPException, ReadTheDocsApi

from app.plugins.gitlab_mr.models.check import Check
from app.plugins.gitlab_mr.models.message_generator import MessageType
from app.plugins.gitlab_mr.models.mrhook import MRHook


class CheckReadthedocsIntegration(Check):
    feature_toggle = "check-readthedocs-integration"

    def __init__(self, api: GitLabApi, rtd_api: ReadTheDocsApi, logger_name):
        self.api = api
        self.rtd_api = rtd_api
        self.logger = logging.getLogger(logger_name)
        self.failing_checks = []

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        # project id is 9070656 (devportal) doesn't need to be checked
        if proj_id != 9070656:
            search_refs = mr_event.object_attributes.source_branch
            mr_search = await self.api.get_search_results(
                proj_id, "blobs", "path:docs/", search_refs
            )
            self.logger.debug("Search Result: %s", mr_search)
            devportal_slug = "developerskatelescopeorg"
            slug_alias = mr_event.project.path_with_namespace
            slug_alias = slug_alias.split("/")[-1]
            hook_data = await self.api.get_project_hooks(proj_id)
            try:
                rtd = await self.rtd_api.get_subproject_detail(
                    devportal_slug, slug_alias
                )
            except HTTPException as ex:
                rtd = None
                self.logger.debug("Project not Found!: %s", ex)

            self.logger.debug("RTD Proj: %s", rtd)
            mr_search_empty = True
            async for _ in mr_search:
                mr_search_empty = False
                break

            if mr_search_empty:
                self.failing_checks.append(
                    "Please set up docs/ folder for sphinx documentation build"
                    " following [the guidelines](https://developer.skatelescope.org/en/latest/tools/documentation.html#documentation-on-git)"  # NOQA: E501
                )

            # Check subproject of dev portal
            if rtd is None:
                self.failing_checks.append(
                    "Please add this project as a subproject on Read the Docs "
                    "following [the guidelines](https://developer.skatelescope.org/en/latest/tools/documentation.html#add-project-as-a-sub-project-on-readthedocs)"  # NOQA: E501
                )

            # Check webhook
            slug = f"https://readthedocs.org/api/.+/webhook/ska-telescope-{slug_alias}"  # NOQA: E501
            reg = re.compile(slug)
            have_hooks = False
            async for regis in hook_data:
                url = regis["url"]
                self.logger.debug("Checking Hook: %s", url)
                if bool(re.match(reg, url)):
                    have_hooks = True
                    break  # Break the for loop to deal with multiple hooks
            if not have_hooks:
                self.failing_checks.append(
                    "Please "
                    "[import](https://developer.skatelescope.org/en/latest/tools/documentation.html#import-project-to-readthedocs) "  # NOQA: E501
                    "your project into Read the Docs"
                )
        return not self.failing_checks

    async def type(self) -> MessageType:
        return MessageType.FAILURE

    async def description(self) -> str:
        return "ReadTheDocs Integration"

    async def mitigation_strategy(self) -> str:
        init_msg = (
            "Please integrate this project with Read the Docs following "
            "[the guidelines](https://developer.skatelescope.org/en/latest/tools/documentation.html#how-to-document)."  # NOQA: E501
        )
        if self.failing_checks:
            docs_checks = "\n".join(
                f" - {check}" for check in self.failing_checks
            )
            docs_checks_mitigation = (
                f"\n\nChange these settings on your MR:\n{docs_checks}"
            )

        return f"{init_msg}{docs_checks_mitigation}"
