import logging
import re

from jira import JIRAError
from ska_cicd_services_api.gitlab import GitLabApi
from ska_cicd_services_api.jira import SKAJira

from app.plugins.gitlab_mr.models.check import Check
from app.plugins.gitlab_mr.models.message_generator import MessageType
from app.plugins.gitlab_mr.models.mrhook import MRHook


class CheckTitleComment(Check):
    feature_toggle = "check-title-comment"

    def __init__(self, api: GitLabApi, jira_api: SKAJira, logger_name):
        self.api = api
        self.jira = jira_api
        self.logger = logging.getLogger(logger_name)

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        await self.jira.authenticate()
        mr_title = mr_event.object_attributes.title
        # self.logger.info("Retrieved MR: %s", mr)
        self.logger.debug("MR Title: %s", mr_title)

        issue_key_match = re.search(r"([A-Z][A-Z0-9_]+-)\d+", mr_title)
        if not issue_key_match:
            self.logger.error(
                "Regular expression failed to get a Jira Issue"
                " in the MR Title string"
            )
            return False
        else:
            possible_issue_key = issue_key_match.group(0)
            self.logger.debug(
                "Possible Issue key found: %s", possible_issue_key
            )
        try:
            issue_obj = await self.jira.get_issue(possible_issue_key)
            self.logger.debug("Returned issue object: %s", issue_obj)
        except JIRAError as ex:
            self.logger.debug("Issues does not exist!, %s", ex)
            issue_obj = None
        return issue_obj is not None

    async def type(self) -> MessageType:
        return MessageType.FAILURE

    async def description(self) -> str:
        return "Missing Jira Ticket ID in MR Title"

    async def mitigation_strategy(self) -> str:
        return (
            "Please change the MR title to include"
            " a valid Jira ticket id (Uppercase)"
        )
