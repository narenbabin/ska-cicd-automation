import logging
import re

from jira import JIRAError
from ska_cicd_services_api.gitlab import GitLabApi
from ska_cicd_services_api.jira import SKAJira

from app.plugins.gitlab_mr.models.check import Check
from app.plugins.gitlab_mr.models.message_generator import MessageType
from app.plugins.gitlab_mr.models.mrhook import MRHook


class CheckCommitMessage(Check):
    feature_toggle = "check-commit-message"

    def __init__(self, api: GitLabApi, jira_api: SKAJira, logger_name):
        self.api = api
        self.jira = jira_api
        self.logger = logging.getLogger(logger_name)
        self.bad_commits = []

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        await self.jira.authenticate()
        mr_commits = await self.api.get_merge_request_commits(proj_id, mr_id)

        async for commit in mr_commits:
            issue_key_match = re.search(
                r"([A-Z][A-Z0-9_]+-)\d+|Merge branch", commit["message"]
            )
            if not issue_key_match:
                self.bad_commits.append(commit["short_id"])
            else:
                possible_issue_key = issue_key_match.group(0)
                self.logger.debug(
                    "Commit-%s, Possible JIRA Key: %s",
                    commit["short_id"],
                    possible_issue_key,
                )
                try:
                    issue_obj = await self.jira.get_issue(possible_issue_key)
                    self.logger.debug(
                        "Commit-%s, Returned JIRA Key: %s",
                        commit["short_id"],
                        issue_obj,
                    )
                    self.logger.debug("Returned JIRA Issue: %s", issue_obj)
                except JIRAError as ex:
                    self.logger.debug("Issues does not exist!: %s", ex)
                    if (
                        possible_issue_key != "Merge branch"
                    ):  # in the case the message is a merge request
                        self.bad_commits.append(commit["short_id"])

        return not self.bad_commits

    async def type(self) -> MessageType:
        return MessageType.WARNING

    async def description(self) -> str:
        return "Missing Jira Ticket ID in commits"

    async def mitigation_strategy(self) -> str:
        init_msg = (
            "Following commit messages violate "
            "[the formatting standards](https://developer.skatelescope.org/en/latest/tools/git.html#committing-code)."  # NOQA: E501
        )
        commits = "\n".join(
            f" - At commit: {commit}" for commit in self.bad_commits
        )
        return f"{init_msg}\n\n{commits}"
