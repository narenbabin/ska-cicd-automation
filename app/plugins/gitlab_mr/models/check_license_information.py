import logging

from ska_cicd_services_api.gitlab import GitLabApi

from app.plugins.gitlab_mr.models.check import Check
from app.plugins.gitlab_mr.models.message_generator import MessageType
from app.plugins.gitlab_mr.models.mrhook import MRHook


class CheckLicenseInformation(Check):
    feature_toggle = "check-license-information"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        license = True
        mr_license = await self.api.get_project_settings_info(proj_id, license)
        self.logger.debug("MR License: %s", mr_license)
        # License keys are extracted from here: https://choosealicense.com/appendix/ # NOQA: E501
        if mr_license["license"] is not None and mr_license["license"][
            "key"
        ] in [
            "bsd-3-clause",
            "apache-2.0",
            "agpl-3.0",
        ]:
            return True
        else:
            return False

    async def type(self) -> MessageType:
        return MessageType.WARNING

    async def description(self) -> str:
        return "Non-compliant License Information"

    async def mitigation_strategy(self) -> str:
        return (
            "Please update the license information according to "
            "[developer portal guidelines](https://developer.skatelescope.org/en/latest/getting-started/projects/licensing.html?highlight=license)"  # NOQA: E501
            " for your project"
        )
