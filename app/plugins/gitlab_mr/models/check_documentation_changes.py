import logging

from ska_cicd_services_api.gitlab import GitLabApi

from app.plugins.gitlab_mr.models.check import Check
from app.plugins.gitlab_mr.models.message_generator import MessageType
from app.plugins.gitlab_mr.models.mrhook import MRHook


def is_documentation_change(path):
    path = path.lower()
    return (
        "docs/src" in path
        or "docs/source" in path
        or "readme.md" in path
        or "readme.rst" in path
        or "changelog.md" in path
        or "changelog.rst" in path
    )


class CheckDocumentationChanges(Check):
    feature_toggle = "check-documentation-changes"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        changed = True
        # project id is 9070656 (devportal) is always a documentation MR
        if proj_id != 9070656:
            mr_diffs = await self.api.get_mr_diff_versions(proj_id, mr_id)
            # every push originates a new version of the MR
            # in principle we just need to check the latest version
            # (it's state is collected)
            async for diff in mr_diffs:
                self.logger.debug("Diff: %s", diff)
                if diff["state"] == "collected":
                    vr_id = diff["id"]
                    break
            vr_info = await self.api.get_single_single_mr_diff_version(
                proj_id, mr_id, vr_id
            )
            self.logger.debug("First version: %s", vr_info)
            vr_diff = vr_info["diffs"]
            self.logger.debug("Version Diffs: %s", vr_diff)
            changed = False
            for regis in vr_diff:
                old, new = regis["old_path"], regis["new_path"]
                if is_documentation_change(old) or is_documentation_change(
                    new
                ):
                    changed = True
        return changed

    async def type(self) -> MessageType:
        return MessageType.INFO

    async def description(self) -> str:
        return "Documentation Changes"

    async def mitigation_strategy(self) -> str:
        return (
            "This MR doesn't introduce any documentation changes. "
            "Please consider updating documentation to reflect your changes"
        )
