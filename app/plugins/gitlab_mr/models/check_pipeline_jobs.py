import base64
import logging

import yaml
from ska_cicd_services_api.gitlab import GitLabApi

from app.plugins.gitlab_mr.models.check import Check
from app.plugins.gitlab_mr.models.message_generator import MessageType
from app.plugins.gitlab_mr.models.mrhook import MRHook


class CheckPipelineJobs(Check):
    feature_toggle = "check-pipeline-jobs"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)
        self.jobs_needed = {
            "ci-metrics": "(https://developer.skatelescope.org/en/latest/tools/ci-cd/continuous-integration.html?highlight=post_step.yml#automated-collection-of-ci-health-metrics-as-part-of-the-ci-pipeline)",  # NOQA: E501
            "container-scanning": "(https://developer.skao.int/en/latest/tools/software-package-release-procedure.html#vulnerability-scanning-of-artefacts-pushed-to-oci-registry)",  # NOQA: E501
        }
        # self.jobs_needed = {
        #     "ci-metrics": "(https://developer.skatelescope.org/en/latest/tools/ci-cd/continuous-integration.html?highlight=post_step.yml#automated-collection-of-ci-health-metrics-as-part-of-the-ci-pipeline)", # NOQA: E501
        #     "helm-publish": "(https://developer.skatelescope.org/en/latest/tools/software-package-release-procedure.html?highlight=helm_publish.yml#package-and-publish-helm-charts-to-the-skao-helm-chart-repository)" # NOQA: E501
        # }
        self.file_exists = True
        self.project_template = "ska-telescope/templates-repository"

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        source_branch = mr_event.object_attributes.source_branch
        ci_cd_path = mr_event.project.ci_config_path
        self.logger.debug("CI/CD Path: %s", ci_cd_path)
        if ci_cd_path == "" or ci_cd_path is None:
            ci_cd_path = ".gitlab-ci.yml"
        else:
            ci_cd_path = ci_cd_path.replace("/", "%2F")
        try:
            get_file = await self.api.get_file_from_repository(
                proj_id, ci_cd_path, source_branch
            )

            file_yaml_format = decode64_to_yaml(
                get_file["content"]
            )  # decode base64 message and return it as yaml
            for include_fields in file_yaml_format["include"]:
                if (
                    include_fields["project"] == self.project_template
                    and include_fields["file"]
                    == "gitlab-ci/includes/post_step.yml"
                ):
                    del self.jobs_needed["ci-metrics"]
                # if (
                #     include_fields["project"] == self.project_template
                #     and include_fields["file"]
                #     == "gitlab-ci/includes/helm_publish.yml"
                # ):
                #     del self.jobs_needed["helm-publish"]
                if (
                    include_fields["project"] == self.project_template
                    and include_fields["file"]
                    == "gitlab-ci/includes/build_push.yml"
                ):
                    del self.jobs_needed["container-scanning"]

        except Exception as inst:
            if str(inst) == "404 File Not Found":
                self.file_exists = False
                self.logger.error("Pipeline file not found")
            elif str(inst) == "'include'":
                self.logger.error("No include field on gitlab-ci file")
            elif str(inst) == "404 Commit Not Found":
                self.logger.error("Branch Not Found")

        return self.file_exists and not self.jobs_needed

    async def type(self) -> MessageType:
        return MessageType.FAILURE

    async def description(self) -> str:
        return "Pipeline Checks"

    async def mitigation_strategy(self) -> str:
        if not self.file_exists:
            return (
                "Please create a "
                "[pipeline](https://developer.skatelescope.org/en/latest/tools/ci-cd.html) "  # NOQA: E501
                "for this MR"
            )
        else:
            missing_jobs = "\n".join(
                f" - [{check}]{link}"
                for check, link in self.jobs_needed.items()
            )
            missing_jobs_mitigation = (
                f"Please add the following jobs: " f"\n{missing_jobs}"
            )
            return missing_jobs_mitigation


def decode64_to_yaml(message):
    base64_bytes = message.encode("ascii")
    message_bytes = base64.b64decode(base64_bytes)
    file_decoded = message_bytes.decode("ascii")
    return yaml.safe_load(file_decoded)
