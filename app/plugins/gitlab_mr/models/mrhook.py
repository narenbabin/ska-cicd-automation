from typing import Any, Dict, List, Optional

from pydantic import BaseModel
from pydantic.networks import AnyHttpUrl


class MRUser(BaseModel):
    id: Optional[int] = None
    name: Optional[str] = None
    username: Optional[str] = None
    avatar_url: Optional[AnyHttpUrl] = None
    email: Optional[str] = None


class MRProject(BaseModel):
    id: Optional[int] = None
    name: Optional[str] = None
    description: Optional[str] = None
    web_url: Optional[AnyHttpUrl] = None
    avatar_url: Optional[AnyHttpUrl] = None
    git_ssh_url: Optional[str] = None
    git_http_url: Optional[AnyHttpUrl] = None
    namespace: Optional[str] = None
    visibility_level: Optional[int] = None
    path_with_namespace: Optional[str] = None
    default_branch: Optional[str] = None
    ci_config_path: Optional[str] = None
    homepage: Optional[AnyHttpUrl] = None
    url: Optional[str] = None
    ssh_url: Optional[str] = None
    http_url: Optional[AnyHttpUrl] = None


class MRRepository(BaseModel):
    name: Optional[str] = None
    url: Optional[str] = None
    description: Optional[str] = None
    homepage: Optional[AnyHttpUrl] = None


class MRCommitAuthor(BaseModel):
    name: Optional[str] = None
    email: Optional[str] = None


class MRCommit(BaseModel):
    id: Optional[str] = None
    message: Optional[str] = None
    title: Optional[str] = None
    timestamp: Optional[str] = None
    url: Optional[AnyHttpUrl] = None
    author: Optional[MRCommitAuthor] = None


class MRAssignee(BaseModel):
    name: Optional[str] = None
    username: Optional[str] = None
    avatar_url: Optional[AnyHttpUrl] = None


class MRParams(BaseModel):
    force_remove_source_branch: Optional[str] = None
    auto_merge_strategy: Optional[str] = None
    should_remove_source_branch: Optional[bool] = None
    commit_message: Optional[str] = None
    squash_commit_message: Optional[str] = None
    sha: Optional[str] = None


class MRObjectAttributes(BaseModel):
    assignee_id: Optional[int] = None
    author_id: Optional[int] = None
    created_at: Optional[str] = None
    description: Optional[str] = None
    head_pipeline_id: Optional[int] = None
    id: Optional[int] = None
    iid: Optional[int] = None
    last_edited_at: Optional[str] = None
    last_edited_by_id: Optional[int] = None
    merge_commit_sha: Optional[str] = None
    merge_error: Optional[str] = None
    merge_params: Optional[MRParams] = None
    merge_status: Optional[str] = None
    merge_user_id: Optional[int] = None
    merge_when_pipeline_succeeds: Optional[bool] = None
    milestone_id: Optional[int] = None
    source_branch: Optional[str] = None
    source_project_id: Optional[int] = None
    state_id: Optional[int] = None
    target_branch: Optional[str] = None
    target_project_id: Optional[int] = None
    time_estimate: Optional[int] = None
    title: Optional[str] = None
    updated_at: Optional[str] = None
    updated_by_id: Optional[int] = None
    url: Optional[AnyHttpUrl] = None
    source: Optional[MRProject] = None
    target: Optional[MRProject] = None
    last_commit: Optional[MRCommit] = None
    work_in_progress: Optional[bool] = None
    total_time_spent: Optional[int] = None
    human_total_time_spent: Optional[int] = None
    human_time_estimate: Optional[int] = None
    assignee_ids: Optional[List[int]] = None
    state: Optional[str] = None
    action: Optional[str] = None


class MRLabel(BaseModel):
    id: Optional[int] = None
    title: Optional[str] = None
    color: Optional[str] = None
    project_id: Optional[int] = None
    created_at: Optional[str] = None
    updated_at: Optional[str] = None
    template: Optional[bool] = None
    description: Optional[str] = None
    type: Optional[str] = None
    group_id: Optional[int] = None


class MRChangesUpdatedById(BaseModel):
    previous: Optional[int] = None
    current: Optional[int] = None


class MRChangesUpdatedAt(BaseModel):
    previous: Optional[str] = None
    current: Optional[str] = None


class MRChange(BaseModel):
    previous: Optional[Any] = None
    current: Optional[Any] = None


class MRHook(BaseModel):
    object_kind: Optional[str] = None
    event_type: Optional[str] = None
    user: Optional[MRUser] = None
    project: Optional[MRProject] = None
    object_attributes: Optional[MRObjectAttributes] = None
    labels: Optional[List[MRLabel]] = None
    changes: Optional[Dict[str, MRChange]] = None
    repository: Optional[MRRepository] = None
    assignees: Optional[List[MRUser]] = None
