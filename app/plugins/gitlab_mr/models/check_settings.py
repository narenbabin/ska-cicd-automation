import logging

from ska_cicd_services_api.gitlab import GitLabApi

from app.plugins.gitlab_mr.models.check import Check
from app.plugins.gitlab_mr.models.message_generator import MessageType
from app.plugins.gitlab_mr.models.mrhook import MRHook


class CheckSettings(Check):
    feature_toggle = "check-mr-settings"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)
        self.failing_maintainer_checks = []
        self.failing_mr_checks = []

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        proj_settings = await self.api.get_project_settings_info(proj_id)
        proj_mr_approvals = (
            await self.api.get_project_merge_request_approvals_info(proj_id)
        )
        mr_approvals = await self.api.get_merge_request_approvals_info(
            proj_id, mr_id
        )
        mr_settings = await self.api.get_merge_request_info(proj_id, mr_id)
        self.logger.debug("Proj Settings: %s", proj_settings)
        self.logger.debug("Proj MR Approvals: %s", proj_mr_approvals)
        self.logger.debug("MR Approvals: %s", mr_approvals)
        self.logger.debug("MR Settings: %s", mr_settings)

        if not mr_settings["reviewers"]:
            self.failing_mr_checks.append(
                "Please appoint one or more people as reviewer(s)"
            )
        if not mr_settings["force_remove_source_branch"]:
            self.failing_mr_checks.append(
                "Please check Delete source branch when MR is accepted."
            )
        if mr_settings["squash"]:
            self.failing_mr_checks.append(
                "Please uncheck Squash commits when MR is accepted."
            )
        if not proj_settings["merge_method"] == "merge":
            self.failing_maintainer_checks.append(
                "Merge Method should be Merge Commit"
            )
        if not proj_settings["resolve_outdated_diff_discussions"]:
            self.failing_maintainer_checks.append(
                "Automatically resolve mr diff discussions should be checked"
            )
        if not proj_settings["printing_merge_request_link_enabled"]:
            self.failing_maintainer_checks.append(
                "Show link to create/view MR when pushing from "
                "the command line should be checked"
            )
        if not proj_settings["remove_source_branch_after_merge"]:
            self.failing_maintainer_checks.append(
                "Enable Delete source branch option by default should be "
                "checked"
            )
        if not proj_settings["only_allow_merge_if_pipeline_succeeds"]:
            self.failing_maintainer_checks.append(
                "Pipelines must succeed should be checked"
            )
        if proj_mr_approvals["disable_overriding_approvers_per_merge_request"]:
            self.failing_mr_checks.append(
                "Override approvers and approvals per MR should be checked"
            )
        if not proj_mr_approvals["reset_approvals_on_push"]:
            self.failing_maintainer_checks.append(
                "Require new approvals when new commits are added"
                " to an MR should be checked"
            )
        if proj_mr_approvals["merge_requests_author_approval"]:
            self.failing_mr_checks.append(
                "Prevent approval of MR by the auther should be checked"
            )
        if mr_approvals["approvals_required"] == 0:
            self.failing_mr_checks.append(
                "Please try to set at least 1 approval rule to ensure merge "
                "request is reviewed and approved"
            )

        return (
            not self.failing_maintainer_checks and not self.failing_mr_checks
        )

    async def type(self) -> MessageType:
        return MessageType.FAILURE

    async def description(self) -> str:
        return "Wrong Merge Request Settings"

    async def mitigation_strategy(self) -> str:
        init_msg = (
            "Reconfigure MR Settings according to "
            "[the guidelines](https://developer.skatelescope.org/en/latest/tools/git.html#merge-requests)."  # NOQA: E501
        )
        mr_checks_mitigation = ""
        proj_mr_checks_mitigation = ""
        if self.failing_mr_checks:
            mr_checks = "\n".join(
                f" - {check}" for check in self.failing_mr_checks
            )
            mr_checks_mitigation = (
                f"\n\nChange these settings on your MR:\n{mr_checks}"
            )
        if self.failing_maintainer_checks:
            proj_mr_checks = "\n".join(
                f" - {check}" for check in self.failing_maintainer_checks
            )

            proj_mr_checks_mitigation = (
                f"\n\nChange these settings on the project level"
                " (Settings -> General)"
                " (You may need Maintainer rights to change these):"
                f"\n{proj_mr_checks}"
            )

        return f"{init_msg}{mr_checks_mitigation}{proj_mr_checks_mitigation}"
