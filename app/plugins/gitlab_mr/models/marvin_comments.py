# import random


class MarvinComments:
    def __init__(self) -> None:
        self.comment_list = [
            (
                "Simple. I got very bored and depressed, so I went and"
                " plugged myself in"
                " to its CI/CD feed. I talked to the pipeline at great"
                " length and"
                " explained my view of the Universe to it ... and it"
                " committed suicide"
            ),
            (
                "The first ten million MRs were the worst...and the"
                " second ten million"
                " MRs, they were the worst too. The third ten million"
                " MRs I didn't enjoy"
                " at all. After that I went into a bit of a decline."
            ),
            "Sorry, did I say something wrong?",
            (
                "Pardon me for breathing, which I never do anyway so I"
                " don't know why "
                "I bother to say it, oh God I'm so depressed."
            ),
            (
                "Here's another one of those self-satisfied doors. Life!"
                " Don't talk to me about life."
            ),
            (
                "Having solved all the major mathematical, physical,"
                " chemical, biological"
                ", sociological, philosophical, etymological,"
                " meteorological and"
                " psychological problems of the Universe except for"
                " his own, three times"
                " over, [Marvin] was severely stuck for something to do,"
                " and had taken up"
                "*Merge Request Reviewing!.*"
            ),
            (
                """Now the world has gone to bed,
                Darkness(MRs) won't engulf my head,
                I can see in infrared,
                How I hate the night(to check)."""
            ),
            (
                """Now I lay me down to sleep,
                Try to count electric sheep(MR heap),
                Sweet dream(check) wishes you can keep,
                How I hate the night(to check)"""
            ),
            (
                "'Don't blame you,' said Marvin and counted five hundred"
                " and ninety-seven"
                " thousand million failed MRs before falling asleep"
                " again a second later."
            ),
            ("Marvin was humming ironically because he hated MRs so much"),
            (
                "Why should I want to make anything up? Life's bad enough"
                " as it is"
                " without wanting to invent(check) any more (MRs) of it."
            ),
            (
                "'Marvin trudged on down the corridor, still moaning( and"
                " checking MRs. )"
                "'...and then of course I've got this terrible pain in all"
                " the diodes"
                " down my left hand side...'"
            ),
            (
                "It's part of the shape of the Universe. I only have to talk"
                " to somebody and they begin to hate me."
            ),
            (
                "'This is Marvin,' he says. 'He eats everything and yells"
                " like a"
                " distressed baby to get attention. I'm goat-sitting him"
                " this summer.'"
            ),
            ("Life! Don't talk to me about life!"),
            (
                "Here I am, brain the size of a planet, and they tell me"
                " to take you up(check MRs)"
                " to the bridge. Call that job satisfaction? 'Cos I don't."
            ),
            (
                """Arthur: [Earth] was a beautiful place.
                Marvin: Did it have oceans?
                Arthur: Oh yes; great, wide rolling blue oceans.
                Marvin: Can't bear oceans."""
            ),
        ]

    def get_random_quote(self):
        # return random.choice(self.comment_list)
        return (
            "Here I am, brain the size of a planet, and they tell me to check"
            " Merge Requests. Call that job satisfaction? 'Cos I don't."
        )
