import logging

from ska_cicd_services_api.gitlab import GitLabApi

from app.plugins.gitlab_mr.models.check import Check
from app.plugins.gitlab_mr.models.message_generator import MessageType
from app.plugins.gitlab_mr.models.mrhook import MRHook


class CheckDockerComposeUsage(Check):
    feature_toggle = "check-docker-compose-usage"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)
        self.file_list = []

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        # project id is 9070656 (devportal) doesn't need to be checked
        if proj_id != 9070656:
            source_branch = mr_event.object_attributes.source_branch
            mr_search = await self.api.get_search_results(
                proj_id, "blobs", "docker-compose", ref=source_branch
            )
            async for search_result in mr_search:
                self.file_list.append(
                    (search_result["path"], search_result["startline"])
                )
            self.logger.debug("Search API result: %s", self.file_list)
            # return true if the list is empty, meaning no docker-compose usage
        return not self.file_list

    async def type(self) -> MessageType:
        return MessageType.WARNING

    async def description(self) -> str:
        return "Docker Compose Usage"

    async def mitigation_strategy(self) -> str:
        init_msg = "Please remove docker-compose from following files."
        files = "\n".join(
            f" - At file: {file[0]} on line {file[1]}"
            for file in self.file_list
        )
        return f"{init_msg}\n\n{files}"
