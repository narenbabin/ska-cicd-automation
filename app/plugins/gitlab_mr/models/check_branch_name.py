import logging
import re

from jira import JIRAError
from ska_cicd_services_api.gitlab import GitLabApi
from ska_cicd_services_api.jira import SKAJira

from app.plugins.gitlab_mr.models.check import Check
from app.plugins.gitlab_mr.models.message_generator import MessageType
from app.plugins.gitlab_mr.models.mrhook import MRHook


class CheckBranchName(Check):
    feature_toggle = "check-branch-name"

    def __init__(self, api: GitLabApi, jira_api: SKAJira, logger_name):
        self.api = api
        self.jira = jira_api
        self.logger = logging.getLogger(logger_name)

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        await self.jira.authenticate()
        source_branch = mr_event.object_attributes.source_branch
        issue_key_match = re.search(r"([a-z][a-z0-9_]+-)\d+", source_branch)
        if not issue_key_match:
            self.logger.error(
                "Regular expression failed to get a Jira Issue"
                " in the MR Title string"
            )
            return False
        else:
            possible_issue_key = issue_key_match.group(0)
        self.logger.debug("Possible JIRA Key: %s", possible_issue_key)
        try:
            issue_obj = await self.jira.get_issue(possible_issue_key)
            self.logger.debug("Returned JIRA Issue: %s", issue_obj)
        except JIRAError as ex:
            self.logger.debug("Issues does not exist!: %s", ex)
            issue_obj = None
        return issue_obj is not None

    async def type(self) -> MessageType:
        return MessageType.FAILURE

    async def description(self) -> str:
        return "Missing Jira Ticket ID in Branch Name"

    async def mitigation_strategy(self) -> str:
        return """
        Branch name should start with a lowercase Jira ticket id.

        Please close this MR, rename your branch and create a new MR.
        """
