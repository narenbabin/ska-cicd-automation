import logging

from ska_cicd_services_api.gitlab import GitLabApi

from app.plugins.gitlab_mr.models.message_generator import MessageGenerator


class CommentExecutor:
    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)

    async def action(self, proj_id, mr_id, comment_text):

        comments = await self.api.get_merge_request_comments(proj_id, mr_id)
        result = "Comment is not found!"

        comment_found = False
        async for comment in comments:
            if MessageGenerator.message_marker in comment["body"]:
                self.logger.info(
                    "Updating existing comment (by default only the first "
                    "found comment(unordered) is updated)..."
                )
                result = await self.api.modify_comment(
                    proj_id, mr_id, comment["id"], comment_text
                )
                comment_found = True
                break
        if not comment_found:
            self.logger.info("Adding a new comment...")
            result = await self.api.add_comment(proj_id, mr_id, comment_text)
        self.logger.debug("Command result: %s", result)
        return result
