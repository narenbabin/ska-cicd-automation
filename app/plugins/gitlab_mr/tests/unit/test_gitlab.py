import asyncio
import collections
import importlib
import json
import logging
import os
import types
from json import JSONDecodeError
from unittest.mock import AsyncMock

import pytest
from fastapi.testclient import TestClient
from gidgetlab.exceptions import GitLabException
from jira.exceptions import JIRAError
from pytest_bdd import given, parsers, scenarios, then
from ska_cicd_services_api.readthedocs import HTTPException

from app.main import app
from app.plugins.gitlab_mr.models.mrhook import MRHook
from app.plugins.gitlab_mr.routers.mrevent import router

# Used to Print
LOGGER = logging.getLogger(__name__)
# Creating a Testclient with main app
client = TestClient(app)
# Creating a Testclient with router members
client_router = TestClient(router)


# Mock Class to hold methods
# This class is populated with dynamically generated methods later
class GitLabApiMock:
    mock = "This is a Mock Class"

    def throw_exception(self, *args, **kwargs):
        raise GitLabException


class JiraApiMock:
    mock = "This is a Mock Class"

    async def authenticate(self):
        pass

    def throw_exception(self, *args, **kwargs):
        raise JIRAError


class ReadTheDocsApiMock:
    mock = "This is a Mock Class"

    def throw_exception(self, *args, **kwargs):
        raise HTTPException(*args, **kwargs)


# Helper functions to configure return values for mocks
def append_pair_to_mock_method(method, key, value, service):
    pytest.mock_service[service].return_dict[method][key] = value
    return pytest.mock_service[service].return_dict[method]


def append_value_to_mock_method(method, value, service):
    pytest.mock_service[service].return_dict[method] = value
    return pytest.mock_service[service].return_dict[method]


async def yield_to_mock_method(method, service):
    results = pytest.mock_service[service].return_dict[method]
    for i in results:
        yield i


def format_value(value):
    try:
        if value != "None":
            value = json.loads(value)
        else:
            value = None
    except JSONDecodeError:
        # If it's not json go with string
        pass
    return value


# References to the services to Mock
serviceList = {
    "gitlab": GitLabApiMock(),
    "jira": JiraApiMock(),
    "rtd": ReadTheDocsApiMock(),
}

pytest.mock_service = {}
pytest.mr_hook_dict = collections.defaultdict(dict)


@pytest.fixture(autouse=True)
def setup_services():
    yield
    pytest.mock_service = {}
    pytest.mr_hook_dict = collections.defaultdict(dict)


# TODO set up mock APIs
# def set_up_mock_apis(api):
#     pass


@given(parsers.parse("Main route"))
def check_server():

    response = client.get("/")
    pytest.confirm = response.status_code


@given(
    parsers.parse(
        "Authenticate {method} http request to {target} with Header "
        "env var name is {token_header_env} and Token env var name "
        "is {token_env}"
    )
)
def good_token_auth(
    token_header_env: str, token_env: str, method: str, target: str
):

    assert (
        token_header_env is not None
    ), "Token Header (token_header_env) is not found on plugins.conf.yaml"

    assert (
        token_env is not None
    ), "Token (token_env) is not found on plugins.conf.yaml"

    token_header = os.getenv(token_header_env)
    token = os.getenv(token_env)

    assert token_header is not None, "Token Header env variable is empty"

    assert token is not None, "Token env variable is empty"

    method = method.lower()
    if method == "post":
        response = client.post(target, headers={token_header: token})
    elif method == "get":
        response = client.get(target, headers={token_header: token})

    pytest.confirm = response.status_code


@given(
    parsers.parse(
        "Authenticate {method} http request to {target} with "
        "Header env var name is {token_header_env} and wrong token"
    )
)
def bad_token_auth(token_header_env: str, method: str, target: str):

    assert (
        token_header_env is not None
    ), "Token Header (token_header_env) is not found on plugins.conf.yaml"

    token_header = os.getenv(token_header_env)

    assert token_header is not None, "Token Header env variable is empty"

    token = "wrong token"

    method = method.lower()
    if method == "post":
        response = client.post(target, headers={token_header: token})
    elif method == "get":
        response = client.get(target, headers={token_header: token})

    pytest.confirm = response.status_code


@given(parsers.parse("Actual post event from json file {file}"))
def http_with_body(file: str):
    hook_info = MRHook.parse_file(path=file)
    response = client_router.post("/events/", json=hook_info.dict())
    pytest.confirm = response.status_code


@then(parsers.parse("Return code {message}"))
def check_message(message):
    assert int(message) == pytest.confirm


@given(parsers.parse("a mock {service} API"))
def setup_bare_mock_spec(service):
    api = serviceList.get(service)
    if api is None:
        assert False, f"Please specify a valid service: {service}"

    # Create the mock class
    mock_api = AsyncMock(api)
    # Save the return dict
    placeholder_return_dict = {}
    mock_api.return_dict = placeholder_return_dict

    # Save the mocked api
    pytest.mock_service[service] = mock_api


@given(parsers.parse("a mock {service} API with {method} method"))
def setup_mock_spec(service, method):
    api = serviceList.get(service)
    if api is None:
        assert False, f"Please specify a valid service: {service}"
    # Create the method and add it to the placeholder class
    # This is needed since we want strict spec check against the API
    namespace = {}

    # I welcome anyone to replace this so linting gods will be satisfied
    # because exec is used! Until then
    # pylint: disable=W0122
    exec(
        f"async def {method}(self): return {{}}",
        namespace,
    )
    async_method = types.MethodType(namespace[method], api)
    setattr(api, method, async_method)
    # Create the mock class
    mock_api = AsyncMock(api)
    # Configure mock by adding the method with an empty {} as return
    # If we don't configure it than it would return a Mock object for methods
    placeholder_return = {}
    attrs = {f"{method}.return_value": placeholder_return}
    mock_api.configure_mock(**attrs)

    # Save the return dict
    placeholder_return_dict = {}
    placeholder_return_dict[method] = {}
    mock_api.return_dict = placeholder_return_dict

    # Save the mocked api
    pytest.mock_service[service] = mock_api


@given(parsers.parse("with {method} method in {service} api"))
def append_mock_spec(method, service):
    api = serviceList.get(service)
    namespace = {}

    # I welcome anyone to replace this so linting gods will be satisfied
    # because exec is used! Until then
    # pylint: disable=W0122
    exec(
        f"async def {method}(self): return {{}}",
        namespace,
    )
    async_method = types.MethodType(namespace[method], api)
    setattr(api, method, async_method)
    pytest.mock_service[service].mock_add_spec(api)
    # Configure mock by adding the method with an empty {} as return
    # If we don't configure it than it would return a Mock object for methods
    placeholder_return = {}
    attrs = {f"{method}.return_value": placeholder_return}
    pytest.mock_service[service].configure_mock(**attrs)
    # Save the return dict
    placeholder_return_dict = {}
    pytest.mock_service[service].return_dict[method] = placeholder_return_dict


@given(
    parsers.parse(
        "append key-value pair of {key} with {value}"
        " for {method} method in {service} api"
    )
)
def append_to_mock(key, value, method, service):
    value = format_value(value)
    result_dict = append_pair_to_mock_method(method, key, value, service)
    attrs = {f"{method}.return_value": result_dict}
    pytest.mock_service[service].configure_mock(**attrs)


@given("append diff from <old_path> to <new_path>")
def append_diff(old_path, new_path):
    append_to_mock(
        "diffs",
        '[{"old_path": "' + old_path + '", "new_path": "' + new_path + '"}]',
        "get_single_single_mr_diff_version",
        "gitlab",
    )


@given(parsers.parse("append {value} for {method} method in {service} api"))
def append_yield_to_mock(value, method, service):
    value = format_value(value)
    append_value_to_mock_method(method, value, service)


@given(
    parsers.parse(
        "finally create generator for {method} method in {service} api"
    )
)
def create_async_generator(method, service):
    result = yield_to_mock_method(method, service)
    attrs = {f"{method}.return_value": result}
    pytest.mock_service[service].configure_mock(**attrs)


@given(parsers.parse("throw exception for {method} method in {service} api"))
def add_exception_to_mock(method, service):
    api = serviceList.get(service)
    attrs = {f"{method}.side_effect": api.throw_exception}
    pytest.mock_service[service].configure_mock(**attrs)


@given(parsers.parse("an MR Hook with field {field} with {value}"))
def populate_mr_hook(field, value):
    value = format_value(value)
    fields = field.split(".")
    dic = pytest.mr_hook_dict
    for field in fields[:-1]:
        dic[field] = {}
        dic = dic[field]
    dic[fields[-1]] = value


@then(parsers.parse("{check_name} should return {result}"))
def check_check(check_name, result):
    mr_hook = MRHook.parse_obj(pytest.mr_hook_dict)
    result = json.loads(result)
    mod = importlib.import_module(f"app.plugins.gitlab_mr.models.{check_name}")
    check_class = getattr(mod, check_name.title().replace("_", ""))
    if "jira" in pytest.mock_service:
        check = check_class(
            pytest.mock_service["gitlab"],
            pytest.mock_service["jira"],
            "pytest-logging",
        )
    elif "rtd" in pytest.mock_service:
        check = check_class(
            pytest.mock_service["gitlab"],
            pytest.mock_service["rtd"],
            "pytest-logging",
        )
    else:
        check = check_class(pytest.mock_service["gitlab"], "pytest-logging")
    assert result == asyncio.run(check.check(mr_hook, 1, 2))


@then(parsers.parse("check_documentation_changes should return <result>"))
def check_documentation_changes_check(result):
    check_check("check_documentation_changes", result)


@then(
    parsers.parse(
        "mr event router should return a string starting with {result}"
    )
)
def check_marvin_mr_event(result):
    mr_hook = MRHook.parse_obj(pytest.mr_hook_dict)
    mod = importlib.import_module("app.plugins.gitlab_mr.routers")
    mr_router = getattr(mod, "mrevent")
    assert asyncio.run(mr_router.new_event(mr_hook)).startswith(result)


scenarios("./gitlab.feature")
# scenarios("./gitlab.feature", "./real_event.feature")
