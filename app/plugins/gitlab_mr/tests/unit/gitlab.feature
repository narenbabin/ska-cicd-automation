Feature: Test Web Functionality

    Scenario: Check Server
        Given Main route
        Then  Return code 200

    Scenario: Check an authorized Authentication with http header token
        Given Authenticate GET http request to /gitlab/mr/testAuth with Header env var name is GITLAB_HEADER and Token env var name is GITLAB_TOKEN
        Then  Return code 200

    Scenario: Check an unauthorized Authentication with http header token
        Given Authenticate GET http request to /gitlab/mr/testAuth with Header env var name is GITLAB_HEADER and wrong token
        Then  Return code 401

    Scenario: Check Assignees with more than 1 assignee
        Given a mock gitlab API
        Given an MR Hook with field assignees with [{"id": "1", "name": "ugur"}]
        Then check_assignees should return true

    Scenario: Check Assignees with 0 assignee
        Given a mock gitlab API
        Given an MR Hook with field assignees with []
        Then check_assignees should return false

    Scenario: Check Branch name with valid jira ticket
        Given a mock gitlab API
        Given an MR Hook with field object_attributes.source_branch with st-600
        And a mock jira API with get_issue method
        And append key-value pair of issue with st-600 for get_issue method in jira api
        Then check_branch_name should return true

    Scenario: Check skip tests with a marvin generated branch
        Given an MR Hook with field object_attributes.source_branch with marvin-quarantine-st-691
        Then mr event router should return a string starting with Skipping checks

    Scenario: Check Branch name with valid jira ticket with numbers
        Given a mock gitlab API
        Given an MR Hook with field object_attributes.source_branch with at2-679
        And a mock jira API with get_issue method
        And append key-value pair of issue with at2-679 for get_issue method in jira api
        Then check_branch_name should return true

    Scenario: Check Branch name with invalid jira ticket
        Given a mock gitlab API
        Given an MR Hook with field object_attributes.source_branch with st-9999
        And a mock jira API with get_issue method
        And throw exception for get_issue method in jira api
        Then check_branch_name should return false

    Scenario: Check Branch name without jira ticket
        Given a mock gitlab API
        Given an MR Hook with field object_attributes.source_branch with no-jira
        And a mock jira API with get_issue method
        And throw exception for get_issue method in jira api
        Then check_branch_name should return false

    Scenario: Check Commit message with valid jira ticket
        Given a mock gitlab API with get_merge_request_commits method
        And append [{"short_id": "FAKEID", "message": "ST-600 Valid message"},{"short_id": "FAKEID2", "message": "ST-600 Valid message 2"}] for get_merge_request_commits method in gitlab api
        And finally create generator for get_merge_request_commits method in gitlab api
        And a mock jira API with get_issue method
        And append key-value pair of issue with ST-600 for get_issue method in jira api
        Then check_commit_message should return true

    Scenario: Check Commit message with valid jira ticket with numbers
        Given a mock gitlab API with get_merge_request_commits method
        And append [{"short_id": "FAKEID", "message": "AT2-679 Valid message"},{"short_id": "FAKEID2", "message": "ST-600 Valid message 2"}] for get_merge_request_commits method in gitlab api
        And finally create generator for get_merge_request_commits method in gitlab api
        And a mock jira API with get_issue method
        And append key-value pair of issue with AT2-679 for get_issue method in jira api
        Then check_commit_message should return true

    Scenario: Check Commit message with invalid jira ticket
        Given a mock gitlab API with get_merge_request_commits method
        And append [{"short_id": "FAKEID", "message": "ST-9999 Invalid message"},{"short_id": "FAKEID2", "message": "ST-9999 Invalid message 2"}] for get_merge_request_commits method in gitlab api
        And finally create generator for get_merge_request_commits method in gitlab api
        And a mock jira API with get_issue method
        And throw exception for get_issue method in jira api
        Then check_commit_message should return false

    Scenario: Check Commit message without jira ticket
        Given a mock gitlab API with get_merge_request_commits method
        And append [{"short_id": "FAKEID", "message": "wrong message"},{"short_id": "FAKEID2", "message": "wrong message 2"}] for get_merge_request_commits method in gitlab api
        And finally create generator for get_merge_request_commits method in gitlab api
        And a mock jira API with get_issue method
        And throw exception for get_issue method in jira api
        Then check_commit_message should return false

    Scenario: Check Docker Compose with docker compose present
        Given a mock gitlab API with get_search_results method
        And an MR Hook with field object_attributes.source_branch with docker-compose-check
        And append [{"path": "FAKEPATH", "startline": "42"}] for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        Then check_docker_compose_usage should return false

    Scenario: Check Docker Compose with no docker compose
        Given a mock gitlab API with get_search_results method
        And an MR Hook with field object_attributes.source_branch with docker-compose-check
        And append [] for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        Then check_docker_compose_usage should return true

    Scenario Outline: Check Documentation updates
        Given a mock gitlab API with get_mr_diff_versions method
        And with get_single_single_mr_diff_version method in gitlab api
        And append diff from <old_path> to <new_path>
        And append [{"state" : "collected", "id": "FAKEID"}] for get_mr_diff_versions method in gitlab api
        And finally create generator for get_mr_diff_versions method in gitlab api
        Then check_documentation_changes should return <result>

        Examples: README files
          | old_path    | new_path    | result |
          | README.md   | new         | true   |
          | README.rst  | new         | true   |
          | readme.md   | new         | true   |
          | readme.rst  | new         | true   |
          |        old  |  README.md  | true   |
          |        old  |  README.rst | true   |
          |        old  |  readme.md  | true   |
          |        old  |  readme.rst | true   |
          |        read |  me.rst     | false  |
          |        read |  me.md      | false  |

        Examples: Changelog files
          | old_path       | new_path      | result |
          | CHANGELOG.md   | new           | true   |
          | CHANGELOG.rst  | new           | true   |
          | changelog.md   | new           | true   |
          | changelog.rst  | new           | true   |
          |        old     | CHANGELOG.md  | true   |
          |        old     | CHANGELOG.rst | true   |
          |        old     | changelog.md  | true   |
          |        old     | changelog.rst | true   |
          |        change  | log.rst       | false  |
          |        change  | log.md        | false  |

        Examples: docs/src changes
          | old_path    | new_path    | result |
          | docs/src    | new         | true   |
          | old         | docs/src    | true   |
          | doc         | s/src       | false  |

       Examples: docs/source changes
          | old_path    | new_path    | result |
          | docs/source | new         | true   |
          | old         | docs/source | true   |

       Examples: No documentation changes
          | old_path    | new_path    | result |
          | old         | new         | false  |
          | new         | old         | false  |


    Scenario: Check License Information with bsd-3-clause license
        Given a mock gitlab API with get_project_settings_info method
        And append key-value pair of license with {"key" : "bsd-3-clause"} for get_project_settings_info method in gitlab api
        Then check_license_information should return true

    Scenario: Check License Information with apache-2.0 license
        Given a mock gitlab API with get_project_settings_info method
        And append key-value pair of license with {"key" : "apache-2.0"} for get_project_settings_info method in gitlab api
        Then check_license_information should return true

    Scenario: Check License Information with agpl-3.0 license
        Given a mock gitlab API with get_project_settings_info method
        And append key-value pair of license with {"key" : "agpl-3.0"} for get_project_settings_info method in gitlab api
        Then check_license_information should return true

    Scenario: Check License Information with unknown license
        Given a mock gitlab API with get_project_settings_info method
        And append key-value pair of license with {"key" : "unknown"} for get_project_settings_info method in gitlab api
        Then check_license_information should return false

    Scenario: Check License Information with no license
        Given a mock gitlab API with get_project_settings_info method
        And append key-value pair of license with None for get_project_settings_info method in gitlab api
        Then check_license_information should return false

    Scenario: Check Pipeline Jobs with post step
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with "aW5jbHVkZToKICAtIHByb2plY3Q6ICdza2EtdGVsZXNjb3BlL3RlbXBsYXRlcy1yZXBvc2l0b3J5JwogICAgZmlsZTogJ2dpdGxhYi1jaS9pbmNsdWRlcy9wb3N0X3N0ZXAueW1sJwo=" for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return false

    Scenario: Check Pipeline Jobs with build_push step
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with "aW5jbHVkZToKICAtIHByb2plY3Q6ICdza2EtdGVsZXNjb3BlL3RlbXBsYXRlcy1yZXBvc2l0b3J5JwogICAgZmlsZTogJ2dpdGxhYi1jaS9pbmNsdWRlcy9idWlsZF9wdXNoLnltbCc=" for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return false

    Scenario: Check Pipeline Jobs with both post and build_push step
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with "aW5jbHVkZToKICAjIENyZWF0ZSBHaXRsYWIgQ0kgYmFkZ2VzIGZyb20gQ0kgbWV0cmljcwogICMgaHR0cHM6Ly9kZXZlbG9wZXIuc2thdGVsZXNjb3BlLm9yZy9lbi9sYXRlc3QvdG9vbHMvY29udGludW91c2ludGVncmF0aW9uLmh0bWwjYXV0b21hdGVkLWNvbGxlY3Rpb24tb2YtY2ktaGVhbHRoLW1ldHJpY3MtYXMtcGFydC1vZi10aGUtY2ktcGlwZWxpbmUKICAtIHByb2plY3Q6ICdza2EtdGVsZXNjb3BlL3RlbXBsYXRlcy1yZXBvc2l0b3J5JwogICAgZmlsZTogJ2dpdGxhYi1jaS9pbmNsdWRlcy9wb3N0X3N0ZXAueW1sJwoKICAjIERvY2tlciBidWlsZCBhbmQgcHVzaCB0byBDQVIKICAtIHByb2plY3Q6ICdza2EtdGVsZXNjb3BlL3RlbXBsYXRlcy1yZXBvc2l0b3J5JwogICAgZmlsZTogJ2dpdGxhYi1jaS9pbmNsdWRlcy9idWlsZF9wdXNoLnltbCcK" for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return true

    Scenario: Check Pipeline Jobs with no include
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with "" for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return false

    Scenario: Check Read the Docs Integration with valid set up
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with valid-rtd
        And with get_search_results method in gitlab api
        And with get_project_hooks method in gitlab api
        And a mock rtd API
        And with get_subproject_detail method in rtd api
        And append key-value pair of content with rtd-subproject for get_subproject_detail method in rtd api
        And append key-value pair of content with "" for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api 
        And append [{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-valid-rtd"}] for get_project_hooks method in gitlab api
        And finally create generator for get_project_hooks method in gitlab api 
        Then check_readthedocs_integration should return true

    Scenario: Check Read the Docs Integration with no subproject
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with valid-rtd
        And with get_search_results method in gitlab api
        And with get_project_hooks method in gitlab api
        And a mock rtd API
        And with get_subproject_detail method in rtd api
        And throw exception for get_subproject_detail method in rtd api
        And append key-value pair of content with "" for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api 
        And append [{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-valid-rtd"}] for get_project_hooks method in gitlab api
        And finally create generator for get_project_hooks method in gitlab api 
        Then check_readthedocs_integration should return false
        
    Scenario: Check Read the Docs Integration with no hooks
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with valid-rtd
        And with get_search_results method in gitlab api
        And with get_project_hooks method in gitlab api
        And a mock rtd API
        And with get_subproject_detail method in rtd api
        And append key-value pair of content with rtd-subproject for get_subproject_detail method in rtd api
        And append key-value pair of content with "" for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        And append [] for get_project_hooks method in gitlab api
        And finally create generator for get_project_hooks method in gitlab api 
        Then check_readthedocs_integration should return false

    Scenario: Check Read the Docs Integration with multiple hooks with one valid hook
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with valid-rtd
        And with get_search_results method in gitlab api
        And with get_project_hooks method in gitlab api
        And a mock rtd API
        And with get_subproject_detail method in rtd api
        And append key-value pair of content with rtd-subproject for get_subproject_detail method in rtd api
        And append key-value pair of content with "" for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        And append [{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-valid-rtd"},{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-nonvalid-rtd1"},{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-nonvalid-rtd-2"}] for get_project_hooks method in gitlab api
        And finally create generator for get_project_hooks method in gitlab api 
        Then check_readthedocs_integration should return true

    Scenario: Check Read the Docs Integration with multiple hooks with no valid hook
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with valid-rtd
        And with get_search_results method in gitlab api
        And with get_project_hooks method in gitlab api
        And a mock rtd API
        And with get_subproject_detail method in rtd api
        And append key-value pair of content with rtd-subproject for get_subproject_detail method in rtd api
        And append key-value pair of content with "" for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        And append [{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-nonvalid-rtd-1"},{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-nonvalid-rtd-2"}] for get_project_hooks method in gitlab api
        And finally create generator for get_project_hooks method in gitlab api 
        Then check_readthedocs_integration should return false

    Scenario: Check Read the Docs Integration with no docs folder
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with valid-rtd
        And with get_search_results method in gitlab api
        And with get_project_hooks method in gitlab api
        And a mock rtd API
        And with get_subproject_detail method in rtd api
        And append key-value pair of content with rtd-subproject for get_subproject_detail method in rtd api
        And append [] for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        And append [{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-valid-rtd"}] for get_project_hooks method in gitlab api
        And finally create generator for get_project_hooks method in gitlab api 
        Then check_readthedocs_integration should return false

    Scenario: Check MR Settings with everything as expected
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        Then check_settings should return true

    Scenario: Check MR Settings with no reviewers
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And append key-value pair of reviewers with None for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        Then check_settings should return false

    Scenario: Check MR Settings with force_remove_source_branch not checked
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with false for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        Then check_settings should return false

    Scenario: Check MR Settings with squash true
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with true for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        Then check_settings should return false

    Scenario: Check MR Settings with merge_method as ff
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with ff for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        Then check_settings should return false

    Scenario: Check MR Settings with resolve_outdated_diff_discussions false
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with false for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        Then check_settings should return false

    Scenario: Check MR Settings with printing_merge_request_link disabled
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        Then check_settings should return false

    Scenario: Check MR Settings with remove_source_branch_after_merge disabled
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with false for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        Then check_settings should return false

    Scenario: Check MR Settings with only_allow_merge_if_pipeline_succeeds false
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with false for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        Then check_settings should return false

    Scenario: Check MR Settings with overriding_approvers_per_merge_request enabled
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        Then check_settings should return false

    Scenario: Check MR Settings with reset_approvals_on_push not set
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        Then check_settings should return false

    Scenario: Check MR Settings with merge_requests_author_approval set
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        Then check_settings should return false

    Scenario: Check MR Settings with no approvals_required
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 0 for get_merge_request_approvals_info method in gitlab api
        Then check_settings should return false

    Scenario: Check MR Title with valid JIRA Id
        Given a mock gitlab API
        And an MR Hook with field object_attributes.title with ST-600
        And a mock jira API with get_issue method
        And append key-value pair of issue with valid for get_issue method in jira api
        Then check_title_comment should return true

    Scenario: Check MR Title with valid JIRA Id with numbers
        Given a mock gitlab API
        And an MR Hook with field object_attributes.title with AT2-679
        And a mock jira API with get_issue method
        And append key-value pair of issue with valid for get_issue method in jira api
        Then check_title_comment should return true

    Scenario: Check MR Title with invalid JIRA Id
        Given a mock gitlab API
        And an MR Hook with field object_attributes.title with ST-9999
        And a mock jira API with get_issue method
        And throw exception for get_issue method in jira api
        Then check_title_comment should return false

    Scenario: Check MR Title with valid JIRA Id with Draft Status
        Given a mock gitlab API
        And an MR Hook with field object_attributes.title with Draft: ST-600:
        And a mock jira API with get_issue method
        And append key-value pair of issue with st-600 for get_issue method in jira api
        Then check_title_comment should return true

    Scenario: Check MR Title with no JIRA Id
        Given a mock gitlab API
        And an MR Hook with field object_attributes.title with no-jira-id
        And a mock jira API with get_issue method
        And throw exception for get_issue method in jira api
        Then check_title_comment should return false

