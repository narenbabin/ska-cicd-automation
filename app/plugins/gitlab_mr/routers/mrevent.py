""" GitLab Route for managing MR web hooks """
import asyncio
import logging
import os

import aiohttp
import cachetools
from fastapi import APIRouter, Request
from ska_cicd_services_api.gitlab import GitLabApi
from ska_cicd_services_api.jira import SKAJira
from ska_cicd_services_api.readthedocs import ReadTheDocsApi
from UnleashClient import UnleashClient

from app.plugins.gitlab_mr.models.check import Check
from app.plugins.gitlab_mr.models.check_assignees import CheckAssignees
from app.plugins.gitlab_mr.models.check_branch_name import CheckBranchName
from app.plugins.gitlab_mr.models.check_commit_message import (
    CheckCommitMessage,
)
from app.plugins.gitlab_mr.models.check_docker_compose_usage import (
    CheckDockerComposeUsage,
)
from app.plugins.gitlab_mr.models.check_documentation_changes import (
    CheckDocumentationChanges,
)
from app.plugins.gitlab_mr.models.check_license_information import (
    CheckLicenseInformation,
)
from app.plugins.gitlab_mr.models.check_pipeline_jobs import CheckPipelineJobs
from app.plugins.gitlab_mr.models.check_readthedocs_integration import (
    CheckReadthedocsIntegration,
)
from app.plugins.gitlab_mr.models.check_settings import CheckSettings
from app.plugins.gitlab_mr.models.check_title_comment import CheckTitleComment
from app.plugins.gitlab_mr.models.comment_executor import CommentExecutor
from app.plugins.gitlab_mr.models.marvin_comments import MarvinComments
from app.plugins.gitlab_mr.models.message_generator import MessageGenerator
from app.plugins.gitlab_mr.models.mrhook import MRHook

logger = logging.getLogger("gunicorn.error")

router = APIRouter()

cache = cachetools.LRUCache(maxsize=512)

project_tags_blocklist = ["external"]


feature_toggler = UnleashClient(
    url=os.environ["UNLEASH_API_URL"],
    app_name=os.getenv("UNLEASH_ENVIRONMENT", default="development"),
    instance_id=os.environ["UNLEASH_INSTANCE_ID"],
    disable_metrics=True,
    disable_registration=True,
)
if "UNLEASH_INACTIVE" not in os.environ:
    feature_toggler.initialize_client()


@router.get("/testAuth")
async def test(request: Request):
    logger.info(request)
    return


@router.post("/events/")
async def new_event(body: MRHook):
    """Triggered when a new merge request is created, an existing merge request
    was updated/merged/closed or a commit is added in the source branch"""
    # Check if this is a Marvin generated merge request (ST-691)
    if body.object_attributes.source_branch.startswith("marvin-quarantine-"):
        return f"Skipping checks, Marvin generated merge request: {body}"

    mr_id = body.object_attributes.iid
    logger.info("Received mr_id: %s", mr_id)
    proj_id = body.project.id
    logger.info("Received proj_id: %s", proj_id)
    mr_state = body.object_attributes.state
    logger.info("MR state: %s", mr_state)

    if mr_state == "merged" or mr_state == "closed":
        logger.info("MR is %s, no check required!", mr_state)
        return "No check is performed: MR is merged/closed"

    async with aiohttp.ClientSession() as session:
        gitlab_api = GitLabApi(session, cache=cache)
        proj_settings = await gitlab_api.get_project_settings_info(proj_id)

    proj_tags = proj_settings["tag_list"]

    matching_tags = list(set(proj_tags) & set(project_tags_blocklist))

    if matching_tags:
        logger.info(
            "No check is performed! Project is in blocklist: %s", matching_tags
        )
        return "No check is performed: Project in blocklist"

    mr_message = MessageGenerator("gunicorn.error")

    async with aiohttp.ClientSession() as rtd_session:
        rtd_api = ReadTheDocsApi(rtd_session)

        jira_api = SKAJira()
        async with aiohttp.ClientSession() as session:
            gitlab_api = GitLabApi(session, cache=cache)
            results = await asyncio.gather(
                perform_check(
                    CheckTitleComment(gitlab_api, jira_api, "gunicorn.error"),
                    mr_message,
                    body,
                    proj_id,
                    mr_id,
                ),
                perform_check(
                    CheckAssignees(gitlab_api, "gunicorn.error"),
                    mr_message,
                    body,
                    proj_id,
                    mr_id,
                ),
                perform_check(
                    CheckBranchName(gitlab_api, jira_api, "gunicorn.error"),
                    mr_message,
                    body,
                    proj_id,
                    mr_id,
                ),
                perform_check(
                    CheckCommitMessage(gitlab_api, jira_api, "gunicorn.error"),
                    mr_message,
                    body,
                    proj_id,
                    mr_id,
                ),
                perform_check(
                    CheckSettings(gitlab_api, "gunicorn.error"),
                    mr_message,
                    body,
                    proj_id,
                    mr_id,
                ),
                perform_check(
                    CheckPipelineJobs(gitlab_api, "gunicorn.error"),
                    mr_message,
                    body,
                    proj_id,
                    mr_id,
                ),
                perform_check(
                    CheckDockerComposeUsage(gitlab_api, "gunicorn.error"),
                    mr_message,
                    body,
                    proj_id,
                    mr_id,
                ),
                perform_check(
                    CheckLicenseInformation(gitlab_api, "gunicorn.error"),
                    mr_message,
                    body,
                    proj_id,
                    mr_id,
                ),
                perform_check(
                    CheckDocumentationChanges(gitlab_api, "gunicorn.error"),
                    mr_message,
                    body,
                    proj_id,
                    mr_id,
                ),
                perform_check(
                    CheckReadthedocsIntegration(
                        gitlab_api, rtd_api, "gunicorn.error"
                    ),
                    mr_message,
                    body,
                    proj_id,
                    mr_id,
                ),
            )

    logger.info("Check Results: %s\n", results)

    if False in results:
        mr_message.pretext = (
            "There seems to be some issues with the Merge Request"
            " (MR), Please review the table below and consult "
            "[the developer portal](https://developer.skatelescope.org/en/latest/tools/git.html#merge-request-quality-checks) "  # NOQA: E501
            "for further information:"
        )
        mr_message.posttext = f'*"{MarvinComments().get_random_quote()}"*'
        comment = await mr_message.get_message()
    else:
        comment = await mr_message.get_well_done_message()
    logger.info("comment: %s\n", comment)

    async with aiohttp.ClientSession() as session:
        gitlab_api = GitLabApi(session, cache=cache)
        executor = CommentExecutor(gitlab_api, "gunicorn.error")
        result = await executor.action(proj_id, mr_id, comment)
    logger.info("Result: %s", result)

    return "MR Checks are performed successfully"


async def perform_check(
    check: Check, mr_message: MessageGenerator, mr_event, proj_id, mr_id
):
    """Perform a check and if the check fails populate an entry in the MR table
    and return False, if the check passes return True

    A check is performed using project id and merge request id
    only if it is enabled

    Args:
        check (Check): check to perform
        mr_message (MessageGenerator): MR Message Generator to populate
        proj_id: Project ID of the Merge Request
        mr_id: Merge Request id (iid field in API call)

    Returns:
        true if check passes (or it is disabled), false if it fails
    """
    if not feature_toggler.is_enabled(
        check.feature_toggle,
        fallback_function=lambda feature_name, context: True,
    ):
        return True  # Return as if check passed

    if not await check.check(mr_event, proj_id, mr_id):
        await mr_message.add_entry(
            await check.type(),
            await check.description(),
            await check.mitigation_strategy(),
        )
        return False
    else:
        return True
