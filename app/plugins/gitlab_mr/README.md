# SKA CI/CD Automation Services MR Checks

This plugin is used to check MR quality and provide feedback on the MR window by making comments and updating them.

It uses FastAPI to create webhook that can be set to listen for the GitLab Projects. The following environment variables must be present, the token should have API access to the project:

```console
PRIVATE_TOKEN=...
REQUESTER=...
JIRA_URL=...
JIRA_USERNAME=...
JIRA_PASSWORD=...
GITLAB_TOKEN=...
GITLAB_HEADER=...
UNLEASH_API_URL=...
UNLEASH_INSTANCE_ID=...
RTD_TOKEN=...
```

## Checks

Each check should have:

- Feature Toggle Name: name of the check for runtime configuration
- Result Type: If the check is not successful, whether it should be marked as FAILURE, WARNING, or INFO
- Description: Brief description about what the check is about
- Mitigation Strategy: How to take corrective action to fix the broken check

Currently the plugin checks the MR are:

<!-- markdownlint-disable -->
<table>
    <tr>
        <th>Type</th>
        <th>Description</th>
        <th>Mitigation</th>
    </tr>
    <tr>
        <td>Failure</td>
        <td>Missing Assignee</td>
        <td>Please assign at least one person for the MR</td>
    </tr>
    <tr>
        <td>Failure</td>
        <td>Source Branch Delete Setting</td>
        <td>Please check "Delete source branch when merge request is accepted.</td>
    </tr>
    <tr>
        <td>Failure</td>
        <td>Missing Jira Ticket ID in MR Title</td>
        <td>Please uncheck Squash commits when merge request is accepted.</td>
    </tr>
    <tr>
        <td>Warning</td>
        <td>Docker-Compose Usage Found</td>
        <td>
            Please remove docker-compose from following files:
            <ul>
                <li>At file: `file_location` on line `line_number`</li>
                <li>At file: `file_location` on line `line_number`</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Failure</td>
        <td>Missing Jira Ticket ID in Branch Name</td>
        <td>Branch Name should start with a Jira ticket</td>
    </tr>
    <tr>
        <td>Warning</td>
        <td>Missing Jira Ticket ID in commits</td>
        <td>
            Following commit messages violate the <a href=https://developer.skatelescope.org/en/latest/tools/git.html#committing-code>formatting standards</a>:
            <ul>
                <li>At commit: `commit_hash`</li>
                <li>At commit: `commit_hash`</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Warning</td>
        <td>Non-compliant License Information</td>
        <td>Please update the license information according to <a href=https://developer.skatelescope.org/en/latest/projects/licensing.html>developer portal guidelines</a> for your project</td>
    </tr>
    <tr>
        <td>Information</td>
        <td>Documentation Changes</td>
        <td>This MR doesn't introduce any documentation changes. Please consider updating documentation to reflect your changes</td>
    </tr>
    <tr>
        <td>Failure</td>
        <td>ReadTheDocs Integration</td>
        <td>
           Please integrate this project with ReadtheDocs following <a href=https://developer.skatelescope.org/en/latest/tools/documentation.html#how-to-document>the guidelines</a>:
            <ul>
                <li>Please set up docs/ folder for sphinx documentation build following <a href=https://developer.skatelescope.org/en/latest/tools/documentation.html#documentation-on-git>the guidelines</a></li>
                <li>Please add this project as a subproject on Read the Docs following <a href=https://developer.skatelescope.org/en/latest/tools/documentation.html#add-project-as-a-sub-project-on-readthedocs>the guidelines</a></li>
                <li>Please <a href=https://developer.skatelescope.org/en/latest/tools/documentation.html#import-project-to-readthedocs>import</a> your project into Read the Docs</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Failure</td>
        <td>Wrong Merge Request Settings</td>
        <td>
            Reconfigure Merge Request Settings according to the <a href="https://developer.skatelescope.org/en/latest/tools/git.html#merge-requests">guidelines</a>:
            <ul>
                <li>MR Settings Checks:</li>
                <li>You should assign one or more people as reviewer(s)</li>
                <li>Automatically resolve mr diff discussions should be checked</li>
                <li>Override approvers and approvals per MR should be checked</li>
                <li>Remove all approvals when new commits are pushed should be checked</li>
                <li>Prevent approval of MR by the author should be checked</li>
                <li>There should be at least 1 approval required</li>
                <li>Please check Delete source branch when merge request is accepted.</li>
                <li>Please uncheck Squash commits when merge request is accepted.</li>
            </ul>
            Project Settings Checks (You may need Maintainer rights to change these):
            <ul>
                <li>Pipelines must succeed should be checked</li>
                <li>Enable Delete source branch option by default should be checked</li>
                <li>Show link to create/view MR when pushing from the command line should be checked</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Failure</td>
        <td>Could not find needed pipeline jobs</td>
        <td>
            <p>Please create a pipeline on this Merge Request</p>
            <p style="text-align:center;">OR</p>                         
            Please add the following jobs:
            <p> 
                <ul>
                    <li>  <a href="https://developer.skatelescope.org/en/latest/tools/ci-cd/continuous-integration.html?highlight=post_step.yml#automated-collection-of-ci-health-metrics-as-part-of-the-ci-pipeline">ci-metrics </a>
                    <li>  <a href="https://developer.skao.int/en/latest/tools/software-package-release-procedure.html#vulnerability-scanning-of-artefacts-pushed-to-oci-registry">container-scanning </a>
                    <li> <a href="https://developer.skatelescope.org/en/latest/tools/software-package-release-procedure.html?highlight=helm_publish.yml#package-and-publish-helm-charts-to-the-skao-helm-chart-repository">helm-publish</a>
                    <li>  <a href="https://developer.skatelescope.org/en/latest/tools/ci-cd/continuous-integration.html?">pages</a>
                </ul>
            </p>
        </td>
    </tr>
</table>
<!-- markdownlint-enable -->

### Runtime Configuration

This service is using feature toggles to determine which checks to enable/disable at the runtime. It uses [Unleash](https://unleash.github.io/) integration provided by GitLab to achieve this.

For the project level configuration, a project could be disabled using `Project Tags/Topics`. The service uses a blocklist to determine whether it should run the checks as well.

#### Precedence of configuration

- Project Level Configuration with Tags/Topics
- Feature Toggle Strategies

## How to Add a New Check

Each new check must use the abstract base class, [Check](models/check.py), to ensure to define its `type`, `description`, `mitigation strategy` and `check` action, which performs the actual checking on the MR and returns a boolean indicating the result.

Base Class:

```python
class Check(ABC):

    feature_toggle: str = NotImplemented

    @abstractmethod
    async def check(self, mr_event, proj_id, mr_id) -> bool:
        pass

    @abstractmethod
    async def type(self) -> MessageType:
        pass

    @abstractmethod
    async def description(self) -> str:
        pass

    @abstractmethod
    async def mitigation_strategy(self) -> str:
        pass
```

Example Check:

```python
class CheckAssigneesComment(Check):
    feature_toggle = "check-assignees-comment"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)

    async def check(self, mr_event, proj_id, mr_id):
        mr = await self.api.get_merge_request_info(proj_id, mr_id)
        self.logger.debug("Retrieved MR: %s", mr)
        return len(mr["assignees"]) > 0

    async def type(self) -> MessageType:
        return MessageType.FAILURE

    async def description(self) -> str:
        return "Missing Assignee"

    async def mitigation_strategy(self) -> str:
        return "Please assign at least one person for the MR"
```

Then the necessary tests for the added checks should be added in [tests](tests/unit/) folder. These tests should get picked up by the main frameworks testing.

Finally, each check should be initialised and called in the [mrevents](routers/mrevent.py) file to be included into the list of checks that are performed for the MRs.
