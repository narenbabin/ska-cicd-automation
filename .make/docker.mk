#
# IMAGE_TO_TEST defines the tag of the Docker image to test
#
ifneq ($(CI_JOB_ID),)
IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/sdi/$(PROJECT):$(VERSION)
else
IMAGE_TO_TEST = $(CAR_OCI_REGISTRY_HOST)/$(PROJECT):$(VERSION)
endif

#
# Never use the network=host mode when running CI jobs, and add extra
# distinguishing identifiers to the network name and container names to
# prevent collisions with jobs from the same project running at the same
# time.
#
ifneq ($(CI_JOB_ID),)
NETWORK_MODE := ska-devops-$(CI_JOB_ID)
CONTAINER_NAME_PREFIX := $(PROJECT)-$(CI_JOB_ID)-
else
CONTAINER_NAME_PREFIX := $(PROJECT)-
endif

#
# Defines a default make target so that help is printed if make is called
# without a target
#
.DEFAULT_GOAL := help

pull:  ## download the application image
	docker pull $(IMAGE_TO_TEST)

interactive:  ## start an interactive session using the project image (caution: R/W mounts source directory to /app)
	docker run --rm -it -p 3000:80 --name=$(CONTAINER_NAME_PREFIX)dev \
	  -e LOG_LEVEL="debug" --env-file=.env -e JSON_CONFIG_PATH="plugins.conf.yaml" -v $(CURDIR)/app:/app $(IMAGE_TO_TEST) /bin/bash

development: ## start an interactive local development with hot-reload enabled
	docker run --rm -it -p 3000:80 --name=$(CONTAINER_NAME_PREFIX)dev \
	  -e LOG_LEVEL="debug" --env-file=.env -e JSON_CONFIG_PATH="plugins.conf.yaml" -v $(CURDIR)/app:/app $(IMAGE_TO_TEST) /start-reload.sh
