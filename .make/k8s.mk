HELM_HOST ?= https://artefact.skao.int## helm host url https
INGRESS ?= nginx## Ingress Class to choose
MARK ?= all

CHARTS ?= ska-cicd-automation## list of charts

CI_PROJECT_PATH_SLUG ?= ska-cicd-automation
CI_ENVIRONMENT_SLUG ?= ska-cicd-automation

CONFIG ?= data/default.yaml
MULTI_CONFIG ?= gitlab_mr.yaml jira_support.yaml nexus_webhook.yaml

.DEFAULT_GOAL := help

k8s: ## Which kubernetes are we connected to
	@echo "Kubernetes cluster-info:"
	@kubectl cluster-info
	@echo ""
	@echo "kubectl version:"
	@kubectl version
	@echo ""
	@echo "Helm version:"
	@helm version --client

namespace: ## create the kubernetes namespace
	@kubectl describe namespace $(KUBE_NAMESPACE) > /dev/null 2>&1 ; \
		K_DESC=$$? ; \
		if [ $$K_DESC -eq 0 ] ; \
		then kubectl describe namespace $(KUBE_NAMESPACE); \
		else kubectl create namespace $(KUBE_NAMESPACE); \
		fi

delete_namespace: ## delete the kubernetes namespace
	@if [ "default" == "$(KUBE_NAMESPACE)" ] || [ "kube-system" == "$(KUBE_NAMESPACE)" ]; then \
	echo "You cannot delete Namespace: $(KUBE_NAMESPACE)"; \
	exit 1; \
	else \
	kubectl describe namespace $(KUBE_NAMESPACE) && kubectl delete namespace $(KUBE_NAMESPACE); \
	fi

package: ## package charts
	@echo "Packaging helm charts. Any existing file won't be overwritten."; \
	mkdir -p ../tmp
	@for i in $(CHARTS); do \
	helm package $${i} --destination ../tmp > /dev/null; \
	done; \
	mkdir -p ../repository && cp -n ../tmp/* ../repository; \
	cd ../repository && helm repo index .; \
	rm -rf ../tmp

clean: ## clean out references to chart tgz's
	@rm -rf ./charts/*/charts/*.tgz ./charts/*/Chart.lock ./charts/*/requirements.lock ./repository/* ./*/*/*/tests/*/__pycache__ ./*/__pycache__ ./*/*/__pycache__ __pycache__ .pytest_cache dist build .coverage

dep-up: ## update dependencies for every charts in the env var CHARTS
	@cd charts; \
	for i in $(CHARTS); do \
	echo "+++ Updating $${i} chart +++"; \
	helm dependency update $${i}; \
	done;

install-chart: clean dep-up namespace## install or upgrade (if it's already installed) the helm chart with name RELEASE_NAME and path UMBRELLA_CHART_PATH on the namespace KUBE_NAMESPACE
	@sed -e 's/CI_PROJECT_PATH_SLUG/$(CI_PROJECT_PATH_SLUG)/' $(UMBRELLA_CHART_PATH)values.yaml > generated_values.yaml; \
	sed -e 's/CI_ENVIRONMENT_SLUG/$(CI_ENVIRONMENT_SLUG)/' generated_values.yaml > values.yaml; \
	helm upgrade --install $(RELEASE_NAME) \
	--set ingress.class=$(INGRESS) \
	--set global.env.jira_url=$(JIRA_URL) \
	--set global.env.jira_username=$(JIRA_USERNAME) \
	--set global.env.jira_password=$(JIRA_PASSWORD) \
	--set global.env.gitlab_api_requester='"$(GITLAB_API_REQUESTER)"' \
	--set global.env.gitlab_api_private_token='$(GITLAB_API_PRIVATE_TOKEN)' \
	--set global.env.gitlab_header=$(GITLAB_HEADER) \
	--set global.env.gitlab_token=$(GITLAB_TOKEN) \
	--set global.env.unleash_api_url=$(UNLEASH_API_URL) \
	--set global.env.unleash_instance_id=$(UNLEASH_INSTANCE_ID) \
	--set global.env.unleash_environment=$(UNLEASH_ENVIRONMENT) \
	--set global.env.slack_signing_secret=$(SLACK_SIGNING_SECRET) \
	--set global.env.slack_client_secret=$(SLACK_CLIENT_SECRET) \
	--set global.env.slack_bot_token=$(SLACK_BOT_TOKEN) \
	--set global.env.rtd_token=$(RTD_TOKEN) \
	--set global.env.google_api_key=$(GOOGLE_API_KEY) \
	--set global.env.google_spreadsheet_id=$(GOOGLE_SPREADSHEET_ID) \
	--set global.env.nexus_hmac_signature_secret=$(NEXUS_HMAC_SIGNATURE_SECRET) \
	--set plugin.configuration='$(CONFIG)' \
	--set global.storageClass='$(STORAGE)' \
	$(CUSTOM_VALUES) \
	--values values.yaml \
	 $(UMBRELLA_CHART_PATH) --namespace $(KUBE_NAMESPACE); \
	 rm generated_values.yaml; \
	 rm values.yaml

multiple-install-charts: clean dep-up namespace ## same as install-chart but for each configuration in input separated by space
	@n=1; \
	for i in $(MULTI_CONFIG); do \
		export CONFIG=data/$$i; \
		export RELEASE_NAME=$(RELEASE_NAME)-$$n; \
		make install-chart; \
		n=`expr $$n + 1`; \
	done

template-chart: clean dep-up## install the helm chart with name RELEASE_NAME and path UMBRELLA_CHART_PATH on the namespace KUBE_NAMESPACE
	@sed -e 's/CI_PROJECT_PATH_SLUG/$(CI_PROJECT_PATH_SLUG)/' $(UMBRELLA_CHART_PATH)values.yaml > generated_values.yaml; \
	sed -e 's/CI_ENVIRONMENT_SLUG/$(CI_ENVIRONMENT_SLUG)/' generated_values.yaml > values.yaml; \
	helm template $(RELEASE_NAME) \
	--set ingress.class=$(INGRESS) \
	$(CUSTOM_VALUES) \
	--set global.env.jira_url=$(JIRA_URL) \
	--set global.env.jira_username=$(JIRA_USERNAME) \
	--set global.env.jira_password=$(JIRA_PASSWORD) \
	--set global.env.gitlab_api_requester='"$(GITLAB_API_REQUESTER)"' \
	--set global.env.gitlab_api_private_token='$(GITLAB_API_PRIVATE_TOKEN)' \
	--set global.env.gitlab_header=$(GITLAB_HEADER) \
	--set global.env.gitlab_token=$(GITLAB_TOKEN) \
	--set global.env.unleash_api_url=$(UNLEASH_API_URL) \
	--set global.env.unleash_instance_id=$(UNLEASH_INSTANCE_ID) \
	--set global.env.unleash_environment=$(UNLEASH_ENVIRONMENT) \
	--set global.env.slack_signing_secret=$(SLACK_SIGNING_SECRET) \
	--set global.env.slack_client_secret=$(SLACK_CLIENT_SECRET) \
	--set global.env.slack_bot_token=$(SLACK_BOT_TOKEN) \
	--set global.env.rtd_token=$(RTD_TOKEN) \
	--set global.env.google_api_key=$(GOOGLE_API_KEY) \
	--set global.env.google_spreadsheet_id=$(GOOGLE_SPREADSHEET_ID) \
	--set global.env.nexus_hmac_signature_secret=$(NEXUS_HMAC_SIGNATURE_SECRET) \
	--set plugin.configuration='$(CONFIG)' \
	--set global.storageClass='$(STORAGE)' \
	--values values.yaml \
	--debug \
	 $(UMBRELLA_CHART_PATH) --namespace $(KUBE_NAMESPACE); \
	 rm generated_values.yaml; \
	 rm values.yaml

multiple-template-chart: clean dep-up ## output all the chart templates
	@n=1; \
	for i in $(MULTI_CONFIG); do \
		export CONFIG=data/$$i; \
		export RELEASE_NAME=$(RELEASE_NAME)-$$n; \
		make template-chart; \
		n=`expr $$n + 1`; \
	done

bounce:
	kubectl rollout -n $(KUBE_NAMESPACE) restart statefulset.apps -l app=$(HELM_CHART)
	echo "WARN: 'make wait' for terminating pods not possible. Use 'make watch'"

uninstall-chart: ## uninstall the chart
	@helm uninstall  $(RELEASE_NAME) --namespace $(KUBE_NAMESPACE) 

multiple-uninstall-charts: ## same as install-chart but for each configuration in input separated by space
	@n=1; \
	for i in $(MULTI_CONFIG); do \
		export RELEASE_NAME=$(RELEASE_NAME)-$$n; \
		make uninstall-chart; \
		n=`expr $$n + 1`; \
	done

reinstall-chart: uninstall-chart install-chart ## reinstall the chart

multiple-reinstall-charts: multiple-uninstall-charts delete_namespace multiple-install-charts

upgrade-chart: install-chart ## upgrade the chart

wait:## wait for pods to be ready
	@echo "Waiting for pods to be ready"
	@date
	@kubectl -n $(KUBE_NAMESPACE) get pods
	@jobs=$$(kubectl get job --output=jsonpath={.items..metadata.name} -n $(KUBE_NAMESPACE)); \
	echo $$jobs; \
	if [ ! -z $$jobs ]; then \
		kubectl wait job --for=condition=complete --timeout=300s $$jobs -n $(KUBE_NAMESPACE); \
	fi
	@kubectl -n $(KUBE_NAMESPACE) wait --for=condition=ready --all --timeout=300s pods || exit 1
	@date

watch:
	watch kubectl get all,pv,pvc,ingress -n $(KUBE_NAMESPACE)

show: ## show the helm chart
	@helm template $(RELEASE_NAME) $(UMBRELLA_CHART_PATH) \
		--namespace $(KUBE_NAMESPACE) \
		--set xauthority="$(XAUTHORITYx)" \
		--set display="$(DISPLAY)"

# chart_lint: dep-up ## lint check the helm chart
# and append to the linting.xml
chart_lint: dep-up ## lint check the helm chart
	@mkdir -p build/reports; \
	HELM_LINT=$$(helm lint $(UMBRELLA_CHART_PATH) --with-subcharts $(CUSTOM_VALUES)); \
	LINTING_OUTPUT=$$(echo $${HELM_LINT} | grep ERROR -c | tail -1); \
	echo "$${HELM_LINT}"; \
	echo "<testsuites><testsuite errors=\"$${LINTING_OUTPUT}\" failures=\"0\" name=\"helm-lint\" skipped=\"0\" tests=\"0\" time=\"0.000\" timestamp=\"$(shell date)\"> </testsuite></testsuites>" > build/reports/linting-chart.xml ;\
	make --no-print-directory join-lint-reports ;\
	exit ${LINTING_OUTPUT}

describe: ## describe Pods executed from Helm chart
	@for i in `kubectl -n $(KUBE_NAMESPACE) get pods -l app=$(HELM_CHART) -o=name`; \
	do echo "---------------------------------------------------"; \
	echo "Describe for $${i}"; \
	echo kubectl -n $(KUBE_NAMESPACE) describe $${i}; \
	echo "---------------------------------------------------"; \
	kubectl -n $(KUBE_NAMESPACE) describe $${i}; \
	echo "---------------------------------------------------"; \
	echo ""; echo ""; echo ""; \
	done

logs: ## show Helm chart POD logs
	@for i in `kubectl -n $(KUBE_NAMESPACE) get pods -l app=$(HELM_CHART) -o=name`; \
	do \
	echo "---------------------------------------------------"; \
	echo "Logs for $${i}"; \
	echo kubectl -n $(KUBE_NAMESPACE) logs $${i}; \
	echo kubectl -n $(KUBE_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"; \
	echo "---------------------------------------------------"; \
	for j in `kubectl -n $(KUBE_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"`; do \
	RES=`kubectl -n $(KUBE_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
	echo "initContainer: $${j}"; echo "$${RES}"; \
	echo "---------------------------------------------------";\
	done; \
	echo "Main Pod logs for $${i}"; \
	echo "---------------------------------------------------"; \
	for j in `kubectl -n $(KUBE_NAMESPACE) get $${i} -o jsonpath="{.spec.containers[*].name}"`; do \
	RES=`kubectl -n $(KUBE_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
	echo "Container: $${j}"; echo "$${RES}"; \
	echo "---------------------------------------------------";\
	done; \
	echo "---------------------------------------------------"; \
	echo ""; echo ""; echo ""; \
	done

# Utility target to install Helm dependencies
helm_dependencies:
	@which helm ; rc=$$?; \
	if [[ $$rc != 0 ]]; then \
	curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3; \
	chmod 700 get_helm.sh; \
	./get_helm.sh; \
	fi; \
	helm version --client

# Utility target to install K8s dependencies
kubectl_dependencies:
	@([ -n "$(KUBE_CONFIG_BASE64)" ] && [ -n "$(KUBECONFIG)" ]) || (echo "unset variables [KUBE_CONFIG_BASE64/KUBECONFIG] - abort!"; exit 1)
	@which kubectl ; rc=$$?; \
	if [[ $$rc != 0 ]]; then \
		curl -L -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/$(KUBERNETES_VERSION)/bin/linux/amd64/kubectl"; \
		chmod +x /usr/bin/kubectl; \
		mkdir -p /etc/deploy; \
		echo $(KUBE_CONFIG_BASE64) | base64 -d > $(KUBECONFIG); \
	fi
	@echo -e "\nkubectl client version:"
	@kubectl version --client
	@echo -e "\nkubectl config view:"
	@kubectl config view
	@echo -e "\nkubectl config get-contexts:"
	@kubectl config get-contexts
	@echo -e "\nkubectl version:"
	@kubectl version

kubeconfig: ## export current KUBECONFIG as base64 ready for KUBE_CONFIG_BASE64
	@KUBE_CONFIG_BASE64=`kubectl config view --flatten | base64`; \
	echo "KUBE_CONFIG_BASE64: $$(echo $${KUBE_CONFIG_BASE64} | cut -c 1-40)..."; \
	echo "appended to: PrivateRules.mak"; \
	echo -e "\n\n# base64 encoded from: kubectl config view --flatten\nKUBE_CONFIG_BASE64 = $${KUBE_CONFIG_BASE64}" >> PrivateRules.mak

helm_test:  ## run Helm chart tests
	@helm test $(RELEASE_NAME) --namespace $(KUBE_NAMESPACE)

help:  ## show this help.
	@echo "make targets:"
	@grep -hE '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -hE '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \?\= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\#\#/  \#/'

smoketest: wait

upload_wrong_dist: 
	TWINE_USERNAME=$(TWINE_USERNAME) TWINE_PASSWORD=$(TWINE_PASSWORD) twine upload --repository-url $(PYPI_REPOSITORY_URL) wrong_dist/*

k8s_test = tar -c post-deployment/ | \
		kubectl run $(TEST_RUNNER) \
		--namespace $(KUBE_NAMESPACE) -i --wait --restart=Never \
		--image-pull-policy=IfNotPresent \
		--env="GITLAB_HEADER=$(GITLAB_HEADER)" \
		--env="GITLAB_TOKEN=$(GITLAB_TOKEN)" \
		--env="NEXUS_URL=$(NEXUS_URL)" \
		--env="NEXUS_API_USERNAME=$(NEXUS_API_USERNAME)" \
		--env="NEXUS_API_PASSWORD=$(NEXUS_API_PASSWORD)" \
		--env="NEXUS_HMAC_SIGNATURE_SECRET=$(NEXUS_HMAC_SIGNATURE_SECRET)" \
		--image=$(IMAGE_TO_TEST) -- \
		/bin/bash -c "mkdir testing && tar xv --directory testing --strip-components 1 --warning=all && cd testing && \
		make MARK=$(MARK) $1 && \
		tar -czvf /tmp/test-results.tgz build && \
		echo '~~~~BOUNDARY~~~~' && \
		cat /tmp/test-results.tgz | base64 && \
		echo '~~~~BOUNDARY~~~~'" \
		2>&1

# run the test function
# save the status
# clean out charts/build dir
# print the logs minus the base64 encoded payload
# pull out the base64 payload and unpack to charts/build/ dir
# base64 payload is given a boundary "~~~~BOUNDARY~~~~" and extracted using perl
# clean up the run to completion container
# exit the saved status
test: ## test the application on K8s
	$(call k8s_test,test); \
		status=$$?; \
		rm -rf build; \
		mkdir -p build; \
		kubectl --namespace $(KUBE_NAMESPACE) logs $(TEST_RUNNER) | \
		perl -ne 'BEGIN {$$on=0;}; if (index($$_, "~~~~BOUNDARY~~~~")!=-1){$$on+=1;next;}; print if $$on % 2;' | \
		base64 -d | tar -xzf -; \
		kubectl --namespace $(KUBE_NAMESPACE) delete pod $(TEST_RUNNER); \
		exit $$status