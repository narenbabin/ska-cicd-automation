# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define the Docker tag 
# for this project. The definition below inherits the standard value for 
# CAR_OCI_REGISTRY_HOST (=artefact.skao.int) and overwrites PROJECT to
# give a final Docker tag of artefact.skao.int/ska-cicd-automation
#
PROJECT = ska-cicd-automation


# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-devops

# RELEASE_NAME is the release that all Kubernetes resources will be labelled
# with
RELEASE_NAME ?= ska-cicd-automation

# UMBRELLA_CHART_PATH Path of the umbrella chart to work with
HELM_CHART ?= ska-cicd-automation
UMBRELLA_CHART_PATH ?= charts/$(HELM_CHART)/
TEST_RUNNER ?= runner-$(CI_JOB_ID)-$(RELEASE_NAME)##name of the pod running the k8s_tests
JSON_CONFIG_PATH?="app/plugins.conf.yaml"

# Helm version
HELM_VERSION = v3.4.0
# kubectl version
KUBERNETES_VERSION = v1.19.2

# Docker, K8s and Gitlab CI variables
# gitlab-runner debug mode - turn on with non-empty value
RDEBUG ?=
# DOCKER_HOST connector to gitlab-runner - local domain socket for shell exec
DOCKER_HOST ?= unix:///var/run/docker.sock
# DOCKER_VOLUMES pass in local domain socket for DOCKER_HOST
DOCKER_VOLUMES ?= /var/run/docker.sock:/var/run/docker.sock
# registry credentials - user/pass/registry - set these in PrivateRules.mak
DOCKER_REGISTRY_USER_LOGIN ?=  ## registry credentials - user - set in PrivateRules.mak
CI_REGISTRY_PASS_LOGIN ?=  ## registry credentials - pass - set in PrivateRules.mak
CI_REGISTRY ?= gitlab.com

# Gitlab and Jira vars: set in PrivateRules.mak and as environment variables
GITLAB_API_PRIVATE_TOKEN ?=  ## Privte Gitlab Access token - generate at https://gitlab.com/-/profile/personal_access_tokens
GITLAB_API_REQUESTER ?=  ## Developer's Gitlab access token - stored as env variable on Gitlab for production
JIRA_URL ?=
JIRA_USERNAME ?=
JIRA_PASSWORD ?=  ## Jira password
SLACK_BOT_TOKEN ?= ## Token for Slack bot - create on Slack admin page
SLACK_SIGNING_SECRET ?= ## Slack signing secret - create on Slack admin page
GITLAB_TOKEN ?=
GITLAB_HEADER ?= X-Gitlab-Token
UNLEASH_API_URL ?=
UNLEASH_INSTANCE_ID ?=
RTD_TOKEN ?= "mandatory"
NEXUS_HMAC_SIGNATURE_SECRET ?= trytoguess

STORAGE ?= standard## Redis StorageClass for Persistent Volume(s)

# Validation alerting vars: set in PrivateRules.mak and as environment variables
SLACK_WEBHOOK_URL ?=


# Run from local image only, requires either a pulled or local image 
# always run "latest" by default in dev environment
CUSTOM_VALUES ?= --set image.pullPolicy=Never \
	--set image.tag=latest

ifneq ($(CI_JOB_ID),)
CUSTOM_VALUES = --set image.repository=$(CI_REGISTRY)/ska-telescope/sdi/$(PROJECT) \
	--set image.tag=$(VERSION)
else
endif

# In production environment get docker image from Nexus (not GitLab)
ENV_CHECK := $(shell echo $(CI_ENVIRONMENT_SLUG) | egrep production)
ifneq ($(ENV_CHECK),)
CUSTOM_VALUES = 
endif

KUBE_CONFIG_BASE64 ?=  ## base64 encoded kubectl credentials for KUBECONFIG
KUBECONFIG ?= /etc/deploy/config ## KUBECONFIG location

# XAUTHORITYx ?= ${XAUTHORITY}
# THIS_HOST := $(shell ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
# DISPLAY := $(THIS_HOST):0

# define private overrides for above variables in here
-include PrivateRules.mak

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/release.mk	
include .make/docker.mk
include .make/k8s.mk

.PHONY: help

requirements: ## Install Dependencies
	poetry install

update-requirements: ## Update lock file with dependency versions
	poetry update

lint: requirements ## Linting
	@mkdir -p build/reports; \
	isort --check-only app/
	black --line-length 79 --check app/
	flake8 --show-source --statistics app/
	pylint --rcfile=.pylintrc --output-format=parseable app/* | tee build/code_analysis.stdout
	pylint --output-format=pylint_junit.JUnitReporter app/* > build/reports/linting-python.xml
	@make --no-print-directory join-lint-reports

# Join different linting reports into linting.xml
# Zero, create linting.xml with empty testsuites
# First, delete newlines from the files for easier parsing
# Second, parse <testsuite> tags in <testsuites> in each file (disregard any attributes in testsuites tag)
# Final, append <testsuite> tags into linting.xml
join-lint-reports:
	@echo -e "<testsuites>\n</testsuites>" > build/reports/linting.xml; \
	for FILE in build/reports/linting-*.xml; do \
	TEST_RESULTS=$$(tr -d "\n" < $${FILE} | \
	sed -e "s/.*<testsuites[^<]*\(.*\)<\/testsuites>.*/\1/"); \
	TT=$$(echo $${TEST_RESULTS} | sed 's/\//\\\//g'); \
	sed -i.x -e "/<\/testsuites>/ s/.*/$${TT}\n&/" build/reports/linting.xml; \
	rm -f build/reports/linting.xml.x; \
	done

apply-formatting: requirements
	isort app/
	black --line-length 79 app/ 

exportlock: ## Exports runtime dependencies to requirements.txt file if needed
	poetry export --without-hashes -f requirements.txt --output requirements.txt
	poetry export --without-hashes --dev -f requirements.txt --output requirements-dev.txt

unit_test: requirements ## Run unit tests
	@mkdir -p build; \
	if [[ -z $$PLUGIN ]]; then \
		test2run=""; \
		for i in app/plugins/*; do \
			test2run="$$test2run $$i/tests/unit"; \
		done; \
	else \
		test2run="app/plugins/$$PLUGIN/tests/unit"; \
	fi; \
	PRIVATE_TOKEN=$(PRIVATE_TOKEN) REQUESTER=$(REQUESTER) \
	GITLAB_TOKEN=$(GITLAB_TOKEN) GITLAB_HEADER=$(GITLAB_HEADER) \
	UNLEASH_API_URL=$(UNLEASH_API_URL) UNLEASH_INSTANCE_ID=$(UNLEASH_INSTANCE_ID) \
	JIRA_PASSWORD=$(JIRA_PASSWORD) JIRA_USERNAME=$(JIRA_USERNAME) JIRA_URL=$(JIRA_URL) \
	SLACK_SIGNING_SECRET=$(SLACK_SIGNING_SECRET) SLACK_BOT_TOKEN=$(SLACK_BOT_TOKEN) \
	GOOGLE_API_KEY=$(GOOGLE_API_KEY) GOOGLE_SPREADSHEET_ID=$(GOOGLE_SPREADSHEET_ID) \
	RTD_TOKEN=$(RTD_TOKEN) \
	JSON_CONFIG_PATH=$(JSON_CONFIG_PATH) \
	NEXUS_HMAC_SIGNATURE_SECRET=$(NEXUS_HMAC_SIGNATURE_SECRET) \
	pytest --cov=app --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml $$test2run --junitxml=build/reports/unit-tests.xml

gitlab_login:
	docker login registry.gitlab.com -u $(DOCKER_REGISTRY_USER_LOGIN) -p $(CI_REGISTRY_PASS_LOGIN)

redeploy-updated-docker-images: ## Build updated Docker images and redeploy on k8s
	eval $$(minikube docker-env); \
	make docker-build DOCKER_BUILD_ARGS=--no-cache; \
	make multiple-reinstall-charts JSON_CONFIG_PATH=$(JSON_CONFIG_PATH);
