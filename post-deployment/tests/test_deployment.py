# import json
# import logging

# import pytest
import requests
import json
import os

def test_get():
    url = 'http://ska-cicd-automation:80/'

    response = requests.get(url)
    assert response.status_code == 200

def test_post_events():
    url = 'http://ska-cicd-automation:80/gitlab/mr/events/'

    with open("resources/event.json", "r") as myfile:
        data = myfile.read()

    obj = json.loads(data)

    token_header = os.getenv("GITLAB_HEADER")
    token = os.getenv("GITLAB_TOKEN")

    headers = {token_header: token}

    response = requests.post(url, json=obj, headers=headers)
    assert response.status_code == 200

def test_hmac_signed_webhook_triggers():
    url = 'http://ska-cicd-automation:80/nexus/webhooks/testAuth'

    with open("resources/payload.json", "r") as myfile:
        data = myfile.read()
    
    secret = os.getenv("NEXUS_HMAC_SIGNATURE_SECRET")

    obj = json.loads(data)

    import hmac, hashlib
    digest_mkr = hmac.new(
        bytes(secret, "utf-8"),
        bytes(json.dumps(obj), "utf-8"),
        hashlib.sha1,
    )
    headers = {'X-Nexus-Webhook-Signature': digest_mkr.hexdigest()}

    response = requests.post(url, json=obj, headers=headers)
    assert response.status_code == 200

